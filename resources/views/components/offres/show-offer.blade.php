<h1 class="mt-5 font-bold mb-3 text-3xl">{{ $offre->titre_poste }}</h1>
<div class="flex items-center mb-20">
    <div class="w-28 h-28">
         @if (optional($offre->user->infoRecruteur)->logo_entreprise)
              <img class="w-full h-full" src="{{ $offre->user->infoRecruteur->logo_entreprise }}" alt="logo de l'entreprise">
         @else
              <img class="w-full h-full" src="/logos/default.avif" alt="logo par defaut">
         @endif
    </div>
    <div class="ml-2">
          <p class="capitalize text-xl"><strong>{{ optional($offre->user->infoRecruteur)->nom_entreprise }}</strong></p>
         <p class="capitalize"><i class="fa-solid fa-location-dot mr-1 text-gray-500"></i>{{ $offre->localisation_poste }}</p>
         <p class="capitalize">
              @foreach ($offre->secteurs as $secteur)
                   {{ $secteur->nom }},&nbsp;
              @endforeach
         </p>
         <p class="uppercase">{{ $offre->type_poste }}</p>
    </div>
</div>

<div class="mb-5">
    <hr>
         @if ($offre->site_candidature)
               <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded uppercase my-4">
                    <a href="{{ $offre->site_candidature }}">Postuler</a>
               </button>
         @elseif(optional(auth()->user())->role == "candidat")
               <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded uppercase my-4">
                    <a href="{{ route('candidat.auth.offres.postulate', ['offre' => $offre->id]) }}">Postuler</a>
               </button>
         @else
               @livewire('offre.postulate-button')
         @endif
    <hr>
</div>
@if (optional($offre->user->infoRecruteur)->description_entreprise)    
    <h2 class="text-1xl font-bold">Description entreprise</h2>
    {!! optional($offre->user->infoRecruteur)->description_entreprise !!}
    <hr class="my-4">
@endif

{!! $offre->description !!}

@if ($offre->salaire)
   <p class="mt-5">Salaire: {{ $offre->salaire }} FCFA</p>
@endif