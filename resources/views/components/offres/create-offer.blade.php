@if (session('success'))
    <div class="bg-green-400 p-3 text-center text-white text-xl mb-3">
            {{ session('success') }}
    </div>
@endif

<div class="mb-4">
    <p class="text-xl mb-2">Les secteurs <span title="obligatoire" class="text-red-500">*</span></p>
    <div class="flex justify-between flex-wrap">
        @foreach ($secteurs as $secteur)
            <label>
                    <input type="checkbox" name="secteurs[]" value="{{ $secteur->id }}">
                    {{ $secteur->nom }}
            </label>
        @endforeach      
    </div>
    @error('secteurs')
        <span class="text-red-400 text-md mt-1">
                <span>{{ $message }}</span>
        </span>
    @enderror
</div>

<div class="mb-4">
    <label for="type_poste" class="block font-semibold text-gray-700 mb-2">Type de poste <span title="obligatoire" class="text-red-500">*</span></label>
    <select name="type_poste" id="type_poste" autofocus>
        <option value="cdd">CDD</option>
        <option value="cdi">CDI</option>
        <option value="stage">STAGE</option>
    </select>
    @error('type_poste')
        <span class="text-red-400 text-md">
                <span>{{ $message }}</span>
        </span>
    @enderror
</div>

<div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
    <!-- Titre poste -->
    <div>
        <label for="titre_poste" class="block font-semibold text-gray-700 mb-2">Titre du poste <span title="obligatoire" class="text-red-500">*</span></label>
        <input 
            id="titre_poste" type="text" name="titre_poste" 
            class="shadow border rounded w-full text-xl @error('titre_poste') invalid @enderror" 
            value="{{ old('titre_poste') }}" 
            autocomplete="titre_poste" autofocus
        >
        @error('titre_poste')
            <span class="text-red-400 text-md">
                <span>{{ $message }}</span>
            </span>
        @enderror
    </div>

    <!-- Localisation de poste -->
    <div>
        <label for="localisation_poste" class="block font-semibold text-gray-700 mb-2">Localisation du poste <span title="obligatoire" class="text-red-500">*</span></label>
        <input 
        id="localisation_poste" type="text" name="localisation_poste" 
        class="shadow border rounded w-full text-xl @error('localisation_poste') invalid @enderror" 
        value="{{ old('localisation_poste') }}" 
        autocomplete="localisation_poste" autofocus
        >
        @error('localisation_poste')
        <span class="text-red-400 text-md">
                <span>{{ $message }}</span>
        </span>
        @enderror
    </div>
</div>

<div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
    <!-- email Candidature -->
    <div>
            <label for="email_candidature" class="block font-semibold text-gray-700 mb-2">Email de candidature</label>
            <input 
                id="email_candidature" type="text" name="email_candidature" 
                class="shadow border rounded w-full text-xl @error('email_candidature') invalid @enderror" 
                value="{{ old('email_candidature') }}" 
                autocomplete="email_candidature"
                autofocus
            >
            @error('email_candidature')
                <span class="text-red-400 text-md">
                    <span>{{ $message }}</span>
                </span>
            @enderror
    </div>

    <!-- salaire -->
    <div>
            <label for="salaire" class="block font-semibold text-gray-700 mb-2">salaire</label>
            <input 
                id="salaire" type="text" name="salaire" 
                class="shadow border rounded w-full text-xl @error('salaire') invalid @enderror" 
                value="{{ old('salaire') }}" 
                autocomplete="salaire" 
                autofocus
            >
            @error('salaire')
                <span class="text-red-400 text-md">
                    <span>{{ $message }}</span>
                </span>
            @enderror
    </div>
</div>

<div class="mb-4">
    <!-- description Offre -->
    <label for="description_offre" class="block font-semibold text-gray-700 mb-2">Description de l'offre <span title="obligatoire" class="text-red-500">*</span></label>
    <textarea 
            name="description" 
            id="description_offre" 
            class="shadow border rounded w-full text-xl" cols="30" rows="10"
    >
            {{ old('description') }}
    </textarea>
    @error('description')
            <span class="text-red-400 text-sm">{{ $message }}</span>
    @enderror
</div>

<div class="flex justify-between align-center">
    <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded">Publier</button>
    {{ $link }}
</div>