@if (!is_null(optional($conversation->messages->last())->id))
    <div class="shadow-md mx-auto rounded border-b-2 hover:border-green-300 cursor-pointer px-3 py-4 mb-3">
        <h5 class="mb-4 text-gray-500">{{  App\Models\User::find($conversation->to)->nom }}</h5>
        <div class="flex items-center justify-between">
            <p class="font-semibold">{{ Illuminate\Support\Str::limit(optional($conversation->messages->last())->contenu, 50) }}</p>
            <p class="font-thin text-gray-500">
                    Envoyé par
                    <strong>
                        {{
                            auth()->user()->id === optional(optional($conversation->messages->last())->user)->id ?
                            'Vous'
                            :
                            optional(optional($conversation->messages->last())->user)->nom
                        }}
                    </strong> 
            </p>
        </div>
    </div>
@endif

