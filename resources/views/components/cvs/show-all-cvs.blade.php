@if ($message = Session::get('success'))
          <div id="alert-border-3" class="flex p-4 mb-4 text-green-800 border-t-4 border-green-300 bg-green-50 dark:text-green-400 dark:bg-gray-800 dark:border-green-800" role="alert">
               <svg class="flex-shrink-0 w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
               </svg>
               <div class="ml-3 text-sm font-medium">
                    {{ $message }}
               </div>
          </div>
     @endif
     <div class="flex justify-end">
        @can('isAdmin')
            <a href="{{ route('admin.auth.cvs.create') }}" type="button" class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                Ajouter
            </a>
        @endcan
        @can('isRecruteur')
            <a href="{{ route('recruteur.auth.cvs.create') }}" type="button" class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                Ajouter
            </a>
        @endcan
     </div>
     <div class="rounded-md grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 xs:grid-cols-1 gap-4 my-4">
          @foreach($cvs as $cv)

          <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                @can('isAdmin')
                    <a href="{{ route('admin.auth.cvs.show', ['cv' => $cv->id]) }}" target="_blank" class="photoCv">
                        <img class="rounded-t-lg" src="{{ Storage::url($cv->image) }}" alt="image cv" />
                    </a>
                @elsecan('isRecruteur')    
                    <a href="{{ route('recruteur.auth.cvs.show', ['cv' => $cv->id]) }}" target="_blank" class="photoCv">
                        <img class="rounded-t-lg" src="{{ Storage::url($cv->image) }}" alt="image cv" />
                    </a>
                @else
                    <a href="{{ route('candidat.auth.cvs.show', ['cv' => $cv->id]) }}" target="_blank" class="photoCv">
                        <img class="rounded-t-lg" src="{{ Storage::url($cv->image) }}" alt="image cv" />
                    </a>
                @endcan
               <div class="p-2 flex justify-center bg-blue-600 dowloadCv">
                    <a href="{{ Storage::url($cv->exemple_cv)}}" download="{{ Storage::url($cv->exemple_cv) }}" type="button" class="bg-white hover:bg-gray-100 font-medium rounded-lg text-md px-5 py-2.5 mr-2">
                         <i class="fa-solid fa-download"></i>
                    </a>
                    @can('recruteur-admin-model', $cv)
                        @if (auth()->user()->role == 'admin')    
                            <form action="{{ route('admin.auth.cvs.destroy', ['cv' => $cv->id]) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="bg-red-500 hover:bg-red-600 font-medium rounded-lg text-md px-5 py-2.5 mr-2">
                                    <i class="fa-solid fa-trash"></i> 
                                </button>
                            </form>
                        @endif    
                        @if (auth()->user()->role == 'recruteur')    
                            <form action="{{ route('recruteur.auth.cvs.destroy', ['cv' => $cv->id]) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="bg-red-500 hover:bg-red-600 font-medium rounded-lg text-md px-5 py-2.5 mr-2">
                                    <i class="fa-solid fa-trash"></i> 
                                </button>
                            </form>
                        @endif    
                    @endcan
                    
               </div>
          </div>
          @endforeach
     </div>