<label for="table-search" class="sr-only">Search</label>
<div class="relative mb-3">
    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
        <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
        </svg>
    </div>

    {{ $searchInput }}
</div>

{{ $old_or_recent }}    

@if (session('success'))
    <div class="mb-4 text-green-500 font-semibold">{{ session('success') }}</div>
@endif

<div class="relative overflow-x-auto shadow-md sm:rounded-lg ">
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-lg text-black font-bold uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="p-4">
                    <div class="flex items-center ">
                        N°
                    </div>
                </th>
                <th scope="col" class="px-6 py-3">
                    Nom et Prénom
                </th>
                <th scope="col" class="px-6 py-3">
                    Email
                </th>
                <th scope="col" colspan="2" class="text-center px-6 py-3">
                    Action
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr class="text-xl">
                    <td class="w-4 p-4">
                        <div>
                            {{ $user->id }}
                        </div>
                    </td>
                    <td class="px-6 py-4">
                        <div class="flex items-center">
                            @if ($user->image)
                                <div class="mx-3">
                                    <img src="{{ Storage::url($user->image) }}" alt="profil par défaut" style="width:30px; height:30px; border-radius:50%;">
                                </div>
                            @endif
                            <span> {{ $user->nom }} {{ $user->prenom }}</span>
                        </div>
                    </td>
                    <td class="px-6 py-4">
                        <div>
                            {{ $user->email }}
                        </div>
                    </td>
                    <td class="px-6 py-4">
                        <button class="bg-blue-500 font-semibold hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                            <a href="{{ route('admin.auth.profils.userProfil', ['user' => $user->id]) }}"><i class="fa-solid fa-eye"></i></a>
                        </button>

                    </td>
                    <td class="px-6 py-4">
                        <form action="{{ route('admin.auth.candidats.destroy', ['user' => $user->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="bg-red-500 font-semibold hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                                <i class="fa-solid fa-trash"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>