<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <!-- CSS FILES -->        
        <link rel="preconnect" href="https://fonts.googleapis.com">
        
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

        <link href="https://fonts.googleapis.com/css2?family=Unbounded:wght@300;400;600;700&display=swap" rel="stylesheet">

        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="css/bootstrap-icons.css" rel="stylesheet">

        <link href="css/templatemo-ebook-landing.css" rel="stylesheet">

        @vite(['resources/css/app.css', 'resources/js/app.js'])

        <style>
            [x-cloak] { display: none !important; }
       </style>        
    </head>
    <body class="antialiased">

        @auth
            @can('isAdmin')
                @include('partials.admin-navbar')
            @elsecan('isRecruteur')
                @include('partials.recruteur-navbar')
            @else
                @include('partials.candidat-navbar')
            @endcan
        @else
            @include('partials.navbar-guest')
        @endauth
    
        <!-- <h1 class="text-2xl container-auth">Welcome page</h1> -->


    <!-- <div class="flex flex-col items-center justify-center h-screen" style="background-size: cover; 
  background-position: center;
  height: 100vh;
  margin: 0;
  padding: 0;
  background-repeat: no-repeat;  background-image:  url('https://media.dogfinance.com/files/articles/optimiser-sa-recherche-emploi-sur-le-web_b.jpg');">
            <h1 class="text-4xl font-bold mb-4">Bienvenue sur notre site de recherche d'emploi</h1>
            <p class="text-lg text-white-600 mb-8">Trouvez le travail de vos rêves dès aujourd'hui !</p>
            <a href="recherche-emploi.html" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Commencer la recherche</a>
  </div> -->
<br><br><br>


    <!-- Section principale -->
    <header class="bg-white-100" style="background-size: cover; 
  background-position: center;
  height: 30vh;
  margin: 0;
  padding: 0;
  background-repeat: no-repeat;  background-image:  url('https://cdn.pixabay.com/photo/2016/07/30/19/47/banner-1557852_960_720.jpg');">
        <div class="container mx-auto pt-16"  >
            <h1 class="text-4xl font-bold text-center" style="color: white;">Trouvez votre emploi idéal</h1>
            <p class="text-lg text-center mt-4" style="color: white;">Parcourez des milliers d'offres d'emploi et postulez en quelques clics.</p>
        </div>
    </header>

    <!-- Section des catégories d'emplois -->
    <section class="bg-gray-200 py-16">
        <div class="container mx-auto">
            <h2 class="text-2xl font-bold text-center">Catégories d'emplois populaires</h2>
            <div class="flex justify-center mt-8">
                <div class="p-4 m-2 bg-white rounded shadow-md">
                    <h3 class="font-bold text-xl">Informatique</h3>
                    <p class="mt-2">Découvrez les offres d'emploi dans le domaine de l'informatique.</p>
                </div>
                <div class="p-4 m-2 bg-white rounded shadow-md">
                    <h3 class="font-bold text-xl">Marketing</h3>
                    <p class="mt-2">Trouvez des emplois dans le secteur du marketing et de la publicité.</p>
                </div>
                <div class="p-4 m-2 bg-white rounded shadow-md">
                    <h3 class="font-bold text-xl">Vente</h3>
                    <p class="mt-2">Consultez les offres d'emploi dans le domaine de la vente et du commerce.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Section à propos -->
    <section class="bg-white py-16">
        <div class="container mx-auto">
            <h2 class="text-2xl font-bold text-center">À propos de notre site</h2>
            <p class="text-center mt-4">Notre site de recherche d'emploi est conçu pour vous aider à trouver facilement les offres d'emploi qui vous intéressent et à postuler en quelques clics. Nous mettons en relation les demandeurs d'emploi et les employeurs afin de faciliter le processus de recrutement.</p>
        </div>
    </section>

    <!-- Section de contact -->
    <section class="bg-gray-900 text-white py-16">
        <div class="container mx-auto">
            <h2 class="text-2xl font-bold text-center">Contactez-nous</h2>
            <p class="text-center mt-4">Si vous avez des questions ou des commentaires, n'hésitez pas à nous contacter.</p>
            <div class="flex justify-center mt-8">
                <input type="text" placeholder="Votre nom" class="p-4 w-1/2 md:w-1/4 border border-gray-300 rounded-l">
                <input type="email" placeholder="Votre adresse e-mail" class="p-4 w-1/2 md:w-1/4 border border-gray-300">
                <button class="bg-blue-500 text-white p-4 rounded-r">Envoyer</button>
            </div>
        </div>
    </section>

  








  
    </body>
</html>
