<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Image de cv</title>
     @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body class="body-show-cv">
     <img class="mt-10" src="{{ Storage::url($cv->image) }}" alt="image cv" />
</body>
</html>
