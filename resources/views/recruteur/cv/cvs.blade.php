@extends('layout-main.auth-main')

@section('content')

<div class="container-auth py-3">
     <h1 class="text-lg font-bold">Section de cvs </h1>
     <x-cvs.show-all-cvs :$cvs />
</div>
@endsection