@extends('layout-main.guest-main')

@section('content')
<div class="container-guest container-guest--margin container-guest--max-width mx-auto rounded-lg border shadow-md p-5">
     <h1 class="text-3xl text-blue-500 mb-6 text-center">Recruteur Inscription</h1>
     <form method="POST" action="{{ route('recruteur.store.register') }}" enctype="multipart/form-data">
         @csrf
     
         @if (session('fail'))
               <div class="bg-red-400 p-3 text-center text-white text-xl mb-3">
                    {{ session('fail') }}
               </div>
         @endif
         
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
              <!-- nom -->
               <div>
                    <label for="nom" class="block font-semibold text-gray-700 mb-2">Nom <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="nom" type="text" name="nom" 
                        class="shadow border rounded w-full text-xl @error('nom') invalid @enderror" 
                        value="{{ old('nom') }}" 
                        autocomplete="nom" 
                        placeholder="Votre nom" autofocus
                    >
                    @error('nom')
                        <span class="text-red-400 text-md">
                            <span>{{ $message }}</span>
                        </span>
                    @enderror
               </div>
               <!-- Prenom -->
               <div>
                    <label for="prenom" class="block font-semibold text-gray-700 mb-2">Prenom <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="prenom" type="text" name="prenom" 
                        class="shadow border rounded w-full text-xl @error('prenom') invalid @enderror" 
                        value="{{ old('prenom') }}" 
                        autocomplete="prenom" 
                        placeholder="Votre prenom" autofocus
                    >
                    @error('prenom')
                        <span class="text-red-400 text-md">
                            <span>{{ $message }}</span>
                        </span>
                    @enderror
               </div>
         </div>
     
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
              <!-- telephone -->
              <div>
                  <label for="telephone" class="block font-semibold text-gray-700 mb-2">telephone</label>
                  <input 
                      id="telephone" type="number" name="telephone" 
                      class="shadow border rounded w-full text-xl @error('telephone') invalid @enderror" 
                      value="{{ old('telephone') }}" 
                      autocomplete="telephone" 
                      placeholder="Votre telephone" autofocus
                  >
                  @error('telephone')
                      <span class="text-red-400 text-md">
                          <span>{{ $message }}</span>
                      </span>
                  @enderror
              </div>
              <!-- adresse -->
              <div>
                   <label for="adresse" class="block font-semibold text-gray-700 mb-2">adresse</label>
                   <input 
                       id="adresse" type="text" name="adresse" 
                       class="shadow border rounded w-full text-xl @error('adresse') invalid @enderror" 
                       value="{{ old('adresse') }}" 
                       autocomplete="adresse" 
                       placeholder="Votre adresse" autofocus
                   >
                   @error('adresse')
                       <span class="text-red-400 text-md">
                           <span>{{ $message }}</span>
                       </span>
                   @enderror
              </div>
         </div>
     
         
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- ville -->
               <div>
                    <label for="ville" class="block font-semibold text-gray-700 mb-2">ville <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                         id="ville" type="text" name="ville" 
                         class="shadow border rounded w-full text-xl @error('ville') invalid @enderror" 
                         value="{{ old('ville') }}" 
                         autocomplete="ville" 
                         placeholder="Votre ville" autofocus
                    >
                    @error('ville')
                         <span class="text-red-400 text-md">
                              <span>{{ $message }}</span>
                         </span>
                    @enderror
               </div>

               <!-- email -->
               <div>
                    <label for="email" class="block font-semibold text-gray-700 mb-2">Email <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="email" type="email" name="email" 
                        class="shadow border rounded w-full text-xl @error('email') invalid @enderror" 
                        value="{{ old('email') }}" 
                        autocomplete="email" 
                        placeholder="Votre email" autofocus
                    >
                    @error('email')
                        <span class="text-red-400 text-md">
                            <span>{{ $message }}</span>
                        </span>
                    @enderror
               </div>
         </div>
     
     
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- Mot de passe -->
               <div>
                    <label for="password" class="block font-semibold text-gray-700 mb-2">Mot de passe <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="password" type="password" name="password" 
                        class="shadow border rounded w-full text-xl @error('password') invalid @enderror" 
                        value="{{ old('password') }}" 
                        autocomplete="password" 
                        placeholder="Votre mot de passe" 
                        autofocus
                    >
                   @error('password')
                        <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
               <!-- Confirmation Mot de passe -->
               <div>
                    <label for="password" class="block font-semibold text-gray-700 mb-2">Confirmation Mot de passe <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="password" type="password" name="password_confirmation" 
                        class="shadow border rounded w-full text-xl @error('password') invalid @enderror" 
                        value="{{ old('password_confirmation') }}" 
                        autocomplete="password_confirmation" 
                        placeholder="Votre mot de passe" 
                        autofocus
                    >
                   @error('password_confirmation')
                        <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
         </div>

         <br><hr><br>
         
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- Nom entreprise -->
               <div> 
                    <label for="nom_entreprise" class="block font-semibold text-gray-700 mb-2">Nom de votre entreprise <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                         id="nom_entreprise" type="text" name="nom_entreprise" 
                         class="shadow border rounded w-full text-xl @error('nom_entreprise') invalid @enderror" 
                         value="{{ old('nom_entreprise') }}" 
                         autocomplete="nom_entreprise" 
                         placeholder="Nom entreprise" 
                         autofocus
                    >
                    @error('nom_entreprise')
                         <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
         </div>
     
          <!-- description entreprise -->
          <div class="mb-4">
               <label for="description_entreprise" class="block font-semibold text-gray-700 mb-2">Description de Votre entreprise</label>
               <textarea 
                    name="description_entreprise" 
                    id="description_entreprise" 
                    class="shadow border rounded w-full text-xl" cols="30" rows="10"
               >
                    {{ old('description_entreprise') }}
               </textarea>
               @error('description_entreprise')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>
         
          <!-- logo -->
          <div class="mb-4">
               <label for="logo_entreprise" class="block font-semibold text-gray-700 mb-2">Logo <span class="text-sm font-normal">(Image: jpeg, png, jpg, gif, svg)</span>:</label>
               <input type="file" name="logo_entreprise" id="logo_entreprise" class="form-control">
               @error('logo_entreprise')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>

          <div class="flex justify-between align-center">
               <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded">S'inscrire</button>
               <a href="{{route('recruteur.login') }}" class="text-blue-400 text-lg hover:text-blue-600">Se connecter</a>
          </div>
     </form>
</div>
@push('scripts')
     <script src="https://cdn.ckeditor.com/ckeditor5/37.1.0/classic/ckeditor.js"></script>
     <script>
         ClassicEditor
             .create( document.querySelector( '#description_entreprise' ) )
             .catch( error => {
                 console.error( error );
             } );
     </script>
@endpush
@endsection