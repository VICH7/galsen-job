@extends('layout-main.guest-main')

@section('content')
<div class="container-guest container-guest--margin mx-auto rounded-lg border shadow-md p-5">
    
    <h1 class="text-3xl text-blue-500 mb-6 text-center">Recruteur Connexion</h1>
    <form method="POST" action="{{ route('recruteur.check.login') }}" class="">
        @csrf
    
        @if (session('fail'))
              <div class="bg-red-400 p-3 text-center text-white text-xl mb-3">
                   {{ session('fail') }}
              </div>
        @endif
        
        <div class="mb-4">
            <label for="email" class="block font-semibold text-gray-700 mb-2">Email</label>
            <input 
                id="email" type="email" name="email" 
                class="shadow border rounded w-full text-xl @error('email') invalid @enderror" 
                value="{{ old('email') }}" 
                autocomplete="email" 
                placeholder="Votre email" autofocus
            >
            @error('email')
                <span class="text-red-400 text-md">
                    <span>{{ $message }}</span>
                </span>
            @enderror
        </div>
    
        <div class="mb-4">
            <label for="password" class="block font-semibold text-gray-700 mb-2">Mot de passe</label>
            <input 
                id="password" type="password" name="password" 
                class="shadow border rounded w-full text-xl @error('password') invalid @enderror" 
                value="{{ old('password') }}" 
                autocomplete="password" 
                placeholder="Votre mot de passe" 
                autofocus
            >
           @error('password')
                <span class="text-red-400 text-sm">{{ $message }}</span>
            @enderror
        </div>
    
        <div class="flex justify-between align-center">
            <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded">Se connecter</button>
            <a href="{{route('recruteur.register') }}" class="text-blue-400 text-lg hover:text-blue-600">S'inscrire</a>
        </div>
        <a href="{{ route('password.request') }}" class="text-blue-400 text-lg hover:text-blue-600">Mot de passe oublié ?</a>
    </form>
</div>
@endsection