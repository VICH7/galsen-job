@extends('layout-main.auth-main')

@section('content')

<div class="container-auth">

     @include('partials.add-offer-button')
     <h1 class="mt-5 text-center underline mb-3 text-2xl">Vos Offres</h1>

     @livewire('offre.mes-offres', ['secteurs' => $secteurs ])
</div>
@endsection
