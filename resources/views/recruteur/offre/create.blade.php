@extends('layout-main.auth-main')

@section('content')

<div class="container-auth container-auth-recruteur--margin">
     <h1 class="mt-5 text-center underline mb-3 text-center text-2xl">Création d'Offres</h1>

     <form method="POST" action="{{ route('recruteur.auth.offres.store') }}" enctype="multipart/form-data">
          @csrf
      
          <x-offres.create-offer :$secteurs>

               <x-slot:link>
                    <a 
                         href="{{route('recruteur.auth.offres') }}" 
                         class="bg-red-500 text-white hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded"
                    >
                         Annuler
                    </a>
               </x-slot>

          </x-offres.create-offer>
      
     </form>

</div>

@push('scripts')
     <script src="https://cdn.ckeditor.com/ckeditor5/37.1.0/classic/ckeditor.js"></script>
     <script>
         ClassicEditor
             .create( document.querySelector( '#description_offre' ) )
             .catch( error => {
                 console.error( error );
             } );
     </script>
@endpush
@endsection
