@extends('layout-main.auth-main')

@section('content')

<div class="container-auth container-auth-recruteur--margin">
     
     @include('partials.add-offer-button')
     
     <x-offres.show-offer :$offre />

     @can('recruteur-admin-model', $offre)
          <div class="flex justify-around">

               @can('mofify', $offre)
                    <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4" title="éditer">
                         <a href="{{ route('recruteur.auth.offres.edit', ['offre' => $offre->id]) }}">
                              <i class="fa-solid fa-pen-to-square text-2xl"></i>
                         </a>
                    </button>
               @endcan

               <button class="bg-green-500 text-white hover:bg-green-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4" title="voir les postulant de cette offre">
                    <a href="{{ route('recruteur.auth.offres.postulants', ['offre' => $offre->id]) }}">
                         <i class="fa-solid fa-handshake text-2xl"></i>
                    </a>
               </button>

               <form action="{{ route('recruteur.auth.offres.destroy', ['offre' => $offre->id]) }}" method="post">
                    @csrf
                    @method('DELETE') 
                    <button class="bg-red-500 text-white hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4" title="supprimer">
                              <i class="fa-solid fa-trash"></i>
                    </button>
               </form>
          </div>
     @endcan
</div>

@endsection
