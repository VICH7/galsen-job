<section>

     <header>
          <h2 class="text-xl font-bold" id="additional-information">
               {{ __("Informations Supplémentaires") }}
           </h2>
     </header>
 
     <form id="send-verification" method="post" action="{{ route('verification.send') }}">
         @csrf
     </form>
 
     <form class="mt-5" action="{{ route('recruteur.auth.profils.updateAdditional') }}" method="POST" enctype="multipart/form-data">
         @csrf
         @method('patch')
 
         <div class="mb-6">
             <label for="logo_entreprise">
                  Logo Entreprise <span class="text-sm font-normal">(Image: jpeg, png, jpg)</span> :
             </label>
             <input type="file" name="logo_entreprise" value="{{ old('logo_entreprise', auth()->user()->image) }}" id="logo_entreprise" />
             @error('logo_entreprise')
                  <div class="text-red-500 mt-1 mb-1">{{ $message }}</div>
             @enderror
        </div>
 
         <div class="relative z-0 w-full mb-6">
             <input type="text" name="nom_entreprise" value="{{ auth()->user()->infoRecruteur->nom_entreprise }}" id="nom_entreprise" class="block py-2.5 px-0 w-full text-md text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" required />
             <label for="nom_entreprise" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                 Nom Entreprise
             </label>
             @error('nom_entreprise')
               <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
             @enderror
         </div>

         <div class="mb-6">
               <label for="description_entreprise" class="block font-semibold text-gray-700 mb-2">Description de Votre entreprise</label>
               <textarea 
                    name="description_entreprise" 
                    id="description_entreprise" 
                    class="shadow border rounded w-full text-xl" cols="30" rows="10"
               >
                    {!! old('description_entreprise', auth()->user()->infoRecruteur->description_entreprise) !!}
               </textarea>
               @error('description_entreprise')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>
 
         <button type="submit" class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">Modifier</button>
         <button class="bg-red-500 text-white hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
             <a href="{{ route('recruteur.auth.profils') }}" enctype="multipart/form-data">
                 Annuler
             </a>
         </button>
 
         {{-- @if (session('status') === 'profile-updated')
             <p x-data="{ show: true }" x-show="show" x-transition x-init="setTimeout(() => show = false, 2000)" class="text-md text-gray-600 dark:text-gray-400">{{ __('Succès du mis à jour.') }}</p>
             @endif --}}
     </form>
    
 </section>

 @push('scripts')
 <script src="https://cdn.ckeditor.com/ckeditor5/37.1.0/classic/ckeditor.js"></script>
 <script>
     ClassicEditor
         .create( document.querySelector( '#description_entreprise' ) )
         .catch( error => {
             console.error( error );
         } );
 </script>
@endpush