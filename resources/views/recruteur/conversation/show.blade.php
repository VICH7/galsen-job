@extends('layout-main.auth-main')

@section('content')

<div class="container-auth">
     @livewire('conversation.show-conversation', ['conversation' => $conversation])
</div>
@endsection