<section>
     <header>
         <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
             {{ __('Mettre à jour le mot de passe') }}
         </h2>
 
         <p class="mt-1 text-sm text-gray-600 dark:text-gray-400">
             {{ __('Assurez-vous que votre compte utilise un mot de passe long et aléatoire pour rester en sécurité.') }}
         </p>
     </header>
     
 
    @if (session('error'))
        <div class="text-red-500" role="alert">
            {{ session('error') }}
        </div>
    @endif
 
    @if (session('success'))
        <div class="text-green-500 mb-2 text-center text-xl">{{ session('success') }}</div>
    @endif
 
     <form method="post" action="{{ route('password.update') }}" class="mt-6 space-y-6">
         @csrf
         @method('put')
 
         <div class="relative z-0 w-full mb-6 group">
             <input 
                 id="current_password" 
                 name="current_password" 
                 type="password" 
                 class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer @error('current_password') border-red-500 @enderror"    
                 required
                 />
             <label 
                 for="current_password" 
                 class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"  
             >
                 Mot de passe actuel
             </label>
 
             @error('current_password')
                 <div class="text-red-600 mt-1 mb-1">{{ $message }}</div>
             @enderror
         </div>
 
         <div class="relative z-0 w-full mb-6 group">
             <input 
                 id="password" 
                 name="password" 
                 type="password" 
                 class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer @error('password') border-red-500 @enderror" 
                 required
             />
             <label for="password" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                 Nouveau mot de passe
             </label>
             @error('password')
                 <div class="text-red-600 mt-1 mb-1">{{ $message }}</div>
             @enderror
         </div>
 
         <div class="relative z-0 w-full mb-6 group">
             <input 
                 id="password_confirmation" 
                 name="password_confirmation" 
                 type="password" 
                 class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"
                 required
             />
             <label for="password_confirmation" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                 Confirmation mot de passe
             </label>
             @error('password_confirmation')
                 <div class="text-red-600 mt-1 mb-1">{{ $message }}</div>
             @enderror
         </div>
 
         <div class="flex items-center gap-4">
             <x-primary-button>{{ __('Modifier') }}</x-primary-button>
 
             @if (session('status') === 'password-updated')
             <p x-data="{ show: true }" x-show="show" x-transition x-init="setTimeout(() => show = false, 2000)" class="text-sm text-gray-600 dark:text-gray-400">{{ __('Saved.') }}</p>
             @endif
         </div>
     </form>
 </section>