<div class="cartes grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-8">
     <p class="card-body border shadow-md cad1">{{ $offers_number }} <i class="fa-solid fa-map offre"></i> </br>
     <strong class="nbr">Nombre d'Offres:</strong></p>
     <p class="card-body border shadow-md cad2">{{ $candidats_number }} <i class="fa-solid fa-users offre"></i> </br>
     <strong class="nbr">Nombre de Candidats</strong></p>
     <p class="card-body border shadow-md cad3">{{ $recruteurs_number }} <i class="fa-solid fa-users offre"></i> </br>
     <strong class="nbr">Nombre de Recruteurs</strong></p>
     <p class="card-body border shadow-md cad4">{{ $secteurs_number }} <i class="fa-solid fa-globe offre"></i> </br>
     <strong class="nbr">Nombre de Secteurs d'offre</strong></p>
</div>
   
