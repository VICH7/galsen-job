<header class="header-auth bg-blue-900">
     <nav
          class="
          nav-auth
          w-full
          flex flex-wrap
          items-center
          justify-between
          py-4
          md:py-2
          px-4
          text-lg text-gray-700
          antialiased
          "
     >
          <div>
               <h1 class="text-3xl">
                    <a href="/">
                         <span class="text-green-600">Galsen</span><span class="text-white">Job</span>
                    </a>
               </h1>
          </div>
          
          <svg
               xmlns="http://www.w3.org/2000/svg"
               id="menu-button"
               class="h-6 w-6 cursor-pointer md:hidden block text-green-500"
               fill="none"
               viewBox="0 0 24 24"
               stroke="currentColor"
          >
               <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M4 6h16M4 12h16M4 18h16"
               />
          </svg>
          
          <div class="hidden w-full md:flex md:items-center md:w-auto md:mr-5 md:pt-2" id="menu">
               <ul
                    class="
                    pt-4
                    text-base text-gray-700
                    md:flex
                    md:justify-between 
                    md:pt-0"
                    
               >
                    {{-- <li class="font-bold md:text-center relative text-white" x-data="{open: false}">
                         <span 
                              class="md:px-3 cursor-pointer {{ (request()->is('recruteur/offres*')) || (request()->is('recruteur/offres/favories*')) || (request()->is('recruteur/offres/mes-offres*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 indent-4 md:indent-0 block md:flex md:flex-col md:items-center md:justify-center" 
                              
                              x-on:click="open = !open" x-on:click.outside="open= false"
                         >
                              <i class="fa-solid fa-user hidden md:block"></i>
                              <span>Offres <i class="fa-solid fa-chevron-down"></i></span>
                         </span>

                         <div 
                              class="absolute left-0 border rounded bg-white py-1 mt-1 md:-ml-6 text-black" 
                              x-cloak x-show="open"
                         >
                              <div class="hover:text-green-500 px-2 py-0.5">
                                   <a href="{{ route('recruteur.auth.offres') }}">Les Offres</a>
                              </div>
                              <div class="hover:text-green-500 px-2 py-0.5"><a href="{{ route('recruteur.auth.offres.favories') }}">Offres Favories</a></div>
                              <div class="hover:text-green-500 px-2 py-0.5"><a href="{{ route('recruteur.auth.offres.mes-offres') }}">Mes Offres</a></div> 
                         </div>
                    </li> --}}

                    <li class="font-bold md:text-center text-white">
                         <a class="md:px-3 {{ (request()->is('recruteur/offres*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 indent-4 md:indent-0 block md:flex md:flex-col md:items-center md:justify-center" href="{{ route('recruteur.auth.offres.mes-offres') }}">
                              <i class="fa-solid fa-briefcase mr-2 md:block"></i>
                              <span>Mes Offres</span>
                         </a>
                    </li>

                    <li class="font-bold md:text-center text-white">
                         <a class="md:px-3 {{ (request()->is('recruteur/conversations*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 indent-4 md:indent-0 block md:flex md:flex-col md:items-center md:justify-center" href="{{ route('recruteur.auth.conversations') }}">
                              <i class="fa-solid fa-message hidden md:block"></i>
                              <span>Conversations</span>
                         </a>
                    </li>

                    <li class="font-bold md:text-center text-white">
                         <a class="md:px-3 {{ (request()->is('recruteur/cvs*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 indent-4 md:indent-0 block md:flex md:flex-col md:items-center md:justify-center" href="{{ route('recruteur.auth.cvs') }}">
                              <i class="fa-solid fa-newspaper hidden md:block"></i>
                              <span>Exemple de cv</span>
                         </a>
                    </li>
                    
                    <li class="font-bold md:text-center relative text-white" x-data="{open: false}">

                         <span 
                              class="md:px-3 cursor-pointer {{ (request()->is('recruteur/moi*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 indent-4 md:indent-0 block md:flex md:flex-col md:items-center md:justify-center" 
                              
                              x-on:click="open = !open" x-on:click.outside="open= false"
                         >
                              @if (auth()->user()->image)
                                   <img src="{{ Storage::url(auth()->user()->image) }}" alt="profil admin par défaut" class="mr-2 md:block w-6">
                              @else
                                   <img src="/avatars/default.png" alt="profil admin par défaut" class="mr-2 md:block w-6" style="width: 1.5rem; heigth: 1.5rem; border-radius:50%;">
                              @endif
                              <span>Vous <i class="fa-solid fa-chevron-down"></i></span>
                         </span>
                         <div 
                              class="absolute left-0 border rounded bg-white py-1 mt-1 md:-ml-6 text-black" 
                              x-cloak x-show="open"
                         >
                              <div class="hover:text-green-500 px-2 py-0.5">
                                   <a href="{{ route('recruteur.auth.profils') }}">Profil</a>
                              </div>
                              <div class="hover:text-green-500 px-2 py-0.5"><a href="{{ route('recruteur.auth.parametres') }}">Paramètre</a></div>
                              <div class="hover:text-green-500 px-2 py-0.5">
                                   <a href="{{route('recruteur.auth.logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Déconnexion</a>
                                   <form action="{{route('recruteur.auth.logout')}}" id="logout-form" method="POST" class="d-none">@csrf</form>
                              </div>
                         </div>
                    </li>
                    
               </ul>
          </div>
     </nav>
</header>