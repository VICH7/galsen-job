<header class="bg-blue-900 header-auth">
     <nav
          class="
          nav-auth
          w-full
          flex flex-wrap
          items-center
          justify-between
          py-4
          md:py-2
          px-4
          text-lg text-gray-700
          antialiased
          "
     >
          <div>
               <h1 class="text-3xl">
                    <a href="/">
                         <span class="text-green-600">Galsen</span><span class="text-white">Job</span>
                    </a>
               </h1>
          </div>
          
          <svg
               xmlns="http://www.w3.org/2000/svg"
               id="menu-button"
               class="h-6 w-6 cursor-pointer md:hidden block text-green-500"
               fill="none"
               viewBox="0 0 24 24"
               stroke="currentColor"
          >
               <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M4 6h16M4 12h16M4 18h16"
               />
          </svg>
          
          <div class="hidden w-full md:flex md:items-center md:w-auto md:mr-5 md:pt-2" id="menu">
               <ul
                    class="
                    pt-4
                    text-base text-gray-700
                    md:flex
                    md:justify-between 
                    md:pt-0"
                    
               >
                    <li class="font-bold md:text-center relative text-white" x-data="{open: false}">
                         <span 
                              class="md:px-3 cursor-pointer  {{  (request()->is('admin/offres*')) || (request()->is('admin/offres/favories*')) || (request()->is('admin/offres/mes-offres*')) ? 'text-green-500' : 'hover:text-green-500'  }}  text-xl py-2 pl-2 block flex items-center md:flex-col md:justify-center" 
                              x-on:click="open = !open" x-on:click.outside="open= false"
                         >
                              <i class="fa-solid fa-briefcase mr-2 md:block"></i>
                              <span>Offres <i class="fa-solid fa-chevron-down"></i></span>
                         </span>
                         <div 
                              class="absolute left-0 border rounded bg-white py-1 mt-1 md:-ml-6 text-black" 
                              x-cloak x-show="open"
                         >
                              <div class="hover:text-green-500 px-4 py-0.5">
                                   <a href="{{ route('admin.auth.offres') }}">Les Offres</a>
                              </div>
                              <div class="hover:text-green-500 px-4 py-0.5">
                                   <a href="{{ route('admin.auth.offres.favories') }}">Offres Favories</a>
                              </div>
                              <div class="hover:text-green-500 px-4 py-0.5">
                                   <a href="{{ route('admin.auth.offres.mes-offres') }}">Mes Offres</a>
                              </div>
                         </div>
                    </li>

                    <li class="font-bold md:text-center text-white">
                         <a href="{{ route('admin.auth.secteurs') }}" class="md:px-3 {{ (request()->is('admin/secteurs*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 pl-2 block flex items-center md:flex-col md:justify-center">
                              <i class="fa-solid fa-vector-square mr-2 md:block"></i>
                              <span class="">Secteurs</span>
                         </a>
                    </li>

                    <li class="font-bold md:text-center text-white">
                         <a class="md:px-3 {{ (request()->is('admin/conversations*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 indent-4 md:indent-0 block md:flex md:flex-col md:items-center md:justify-center" href="{{ route('admin.auth.conversations') }}">
                              <i class="fa-solid fa-message hidden md:block"></i>
                              <span>Conversations</span>
                         </a>
                    </li>
                    
                    <li class="font-bold md:text-center relative text-white" x-data="{open: false}">
                         <span 
                              class="md:px-3 cursor-pointer {{ (request()->is('admin/candidats*')) || (request()->is('admin/recruteurs*')) ? 'text-green-500' : 'hover:text-green-500'  }}  text-xl py-2 pl-2 block flex items-center md:flex-col md:justify-center" 
                              x-on:click="open = !open" x-on:click.outside="open= false"
                         >
                              <i class="fa-solid fa-users mr-2 md:block"></i>
                              <span>Utilisateurs <i class="fa-solid fa-chevron-down"></i></span>
                         </span>
                         <div 
                              class="absolute left-0 border rounded bg-white py-1 mt-1 md:-ml-6 text-black" 
                              x-cloak x-show="open"
                         >
                              <div class="hover:text-green-500 px-4 py-0.5">
                                   <a href="{{ route('admin.auth.candidats') }}">Candidats</a>
                              </div>
                              <div class="hover:text-green-500 px-4 py-0.5">
                                   <a href="{{ route('admin.auth.recruteurs') }}">Recruteurs</a>
                              </div>
                         </div>
                    </li>

                    <li class="font-bold md:text-center text-white">
                         <a href="{{ route('admin.auth.cvs') }}" class="md:px-3 {{ (request()->is('admin/cvs*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 pl-2 block flex items-center md:flex-col md:justify-center">
                              <i class="fa-solid fa-newspaper mr-2 md:block"></i>
                              <span>Exemple CV</span>
                         </a>
                    </li>

                    <li class="font-bold md:text-center relative text-white" x-data="{open: false}">
                         <span 
                              class="md:px-3 cursor-pointer {{ (request()->is('admin/moi*')) ? 'text-green-500' : 'hover:text-green-500' }} text-xl py-2 pl-2 block flex items-center md:flex-col md:justify-center" 
                              
                              x-on:click="open = !open" x-on:click.outside="open= false"
                         >    
                                   @if (auth()->user()->image)
                                        <img src="{{ Storage::url(auth()->user()->image) }}" alt="profil admin par défaut" class="mr-2 md:block w-6">
                                   @else
                                        <img src="/avatars/default.png" alt="profil admin par défaut" class="mr-2 md:block w-6" style="width: 1.5rem; heigth: 1.5rem; border-radius:50%;">
                                   @endif
                              
                              <span>Vous <i class="fa-solid fa-chevron-down"></i></span>
                         </span>
                         <div 
                              class="absolute left-0 border rounded bg-white py-1 mt-1 md:-ml-6 text-black" 
                              x-cloak x-show="open"
                         >
                              <div class="hover:text-green-500 px-2 py-0.5">
                                   <a href="{{ route('admin.auth.profils') }}">Profil</a>
                              </div>
                              <div class="hover:text-green-500 px-2 py-0.5"><a href="{{ route('admin.auth.parametres') }}">Paramètre</a></div>
                              <div class="hover:text-green-500 px-2 py-0.5">
                                   <a href="{{route('admin.auth.logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Déconnexion</a>
                                   <form action="{{route('admin.auth.logout')}}" id="logout-form" method="POST" class="d-none">@csrf</form>
                              </div>
                         </div>
                    </li>
                    
               </ul>
          </div>
     </nav>
</header>