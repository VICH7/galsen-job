<div class="flex justify-center py-2 border-b-2 border-slate-500">
     <button class="bg-blue-500 p-2 rounded text-white hover:bg-blue-600">
          @can('isAdmin')
               <a href="{{ route('admin.auth.offres.create') }}">Publier une offre</a>
          @endcan

          @can('isRecruteur')
               <a href="{{ route('recruteur.auth.offres.create') }}">Publier une offre</a>
          @endcan
     </button>
</div>