<footer class="bg-blue-900 text-white">
    <div class="container mx-auto py-6">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-6">
            <div class="col-span-1">
                <h3 class="text-lg font-semibold mb-4">À propos de SenJob</h3>
                <p class="text-sm mb-2">SenJob est un site de recherche d'emploi permettant de trouver des offres correspondantes à vos compétences et préférences.</p>
                <p class="text-sm">Trouvez le travail qui vous correspond !</p>
            </div>
            <div class="col-span-1">
                <h3 class="text-lg font-semibold mb-4">Liens utiles</h3>
                <ul class="list-none">
                    <li class="mb-2"><a href="#" class="text-white">Accueil</a></li>
                    <li class="mb-2"><a href="#" class="text-white">Offres d'emploi</a></li>
                    <li class="mb-2"><a href="#" class="text-white">Entreprises</a></li>
                    <li class="mb-2"><a href="#" class="text-white">À propos</a></li>
                    <li class="mb-2"><a href="#" class="text-white">Contact</a></li>
                </ul>
            </div>
            <div class="col-span-1">
                <h3 class="text-lg font-semibold mb-4">Services</h3>
                <ul class="list-none">
                    <li class="mb-2"><a href="#" class="text-white">Créer un compte</a></li>
                    <li class="mb-2"><a href="#" class="text-white">Se connecter</a></li>
                    <li class="mb-2"><a href="#" class="text-white">Recherche avancée</a></li>
                    <li class="mb-2"><a href="#" class="text-white">Conseils carrière</a></li>
                </ul>
            </div>
            <div class="col-span-1">
                <h3 class="text-lg font-semibold mb-4">Contact</h3>
                <p class="text-sm mb-2">Adresse : 123 Rue du Travail, Dakar, Sénégal</p>
                <p class="text-sm mb-2">Téléphone : +221 123 456 789</p>
                <p class="text-sm">Email : contact@senjob.com</p>
            </div>
        </div>
    </div>

   
    
    <div>
    <div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.9318925296767!2d-17.45263858523252!3d14.695459781690196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xe393e6224b6e2d3%3A0x6e0f3bde9cfc9e8e!2sDakar%2C%20S%C3%A9n%C3%A9gal!5e0!3m2!1sen!2s!4v1655706487077!5m2!1sen!2s" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

    </div>

    <div class="bg-gray-800 py-2">
        <div class="container mx-auto text-center text-sm">
        <p class="text-white">Tous droits réservés &copy; {{ date('Y') }} SenJob</p>
        </div>
    </div>

</footer>
