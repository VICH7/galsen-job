<header class="header--guest bg-blue-900"
>
     <nav
          class="
          nav-guest
          w-full
          flex flex-wrap
          items-center
          justify-between
          py-4
          md:py-2
          px-4
          text-lg text-gray-700
          antialiased
          "
     >
          <div>
               <h1 class="text-3xl">
                    <a href="/">
                         <span class="text-green-600">Galsen</span><span class="text-white">Job</span>
                    </a>
               </h1>
          </div>
          
          <svg
               xmlns="http://www.w3.org/2000/svg"
               id="menu-button"
               class="h-6 w-6 cursor-pointer md:hidden block text-green-500"
               fill="none"
               viewBox="0 0 24 24"
               stroke="currentColor"
          >
               <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M4 6h16M4 12h16M4 18h16"
               />
          </svg>
          
          <div class="hidden w-full md:flex md:items-center md:w-auto md:mr-5 md:pt-2" id="menu">
               <ul
                    class="
                    pt-4
                    text-base text-gray-700
                    md:flex
                    md:justify-between 
                    md:pt-0"
                    
               >
                    <li>
                         <a class="md:p-3 py-2 indent-4 md:indent-0 hover:text-green-500  text-white block" href="{{ route('guest-offers') }}">Offre emploi</a>
                    </li>
                    <li class="md:mx-3">
                         <a class="md:p-3 py-2 indent-4 md:indent-0 hover:text-green-500  text-white block block" href="{{ route('candidat.login') }}">Candidat</a>
                    </li>
                    <li>
                         <a class="md:p-3 py-2 indent-4 md:indent-0 hover:text-green-500  text-white bloc block" href="{{ route('recruteur.login') }}">Recruteur</a>
                    </li>
                    
               </ul>
          </div>
     </nav>
</header>