<div>
    <x-users.users-list :users="$recruteurs" :$old>
        
        <x-slot:searchInput>
            <input wire:model="search" type="search" id="table-search-users" class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-60 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Chercher">
        </x-slot>

        <x-slot:old_or_recent>
            <p class="text-xl cursor-pointer mb-4" wire:click="old_or_recent">       
                @if ($old)
                    <i class="fa-solid fa-arrow-down-short-wide"></i>
                    <span>Récent</span>   
                @else
                    <i class="fa-solid fa-arrow-up-short-wide"></i>
                    <span>Ancient</span>
                @endif
            </p>
        </x-slot>

    </x-users.users-list>
    
    <div class="mt-5">{{ $recruteurs->links() }}</div>
</div>
