<div>
    <div class="flex items-center justify-between py-4 bg-white dark:bg-gray-800">
        <label for="table-search" class="sr-only">Search</label>
        <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                    </svg>
                </div>
                <input wire:model="search" type="search" id="table-search-users" class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-60 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Chercher">
        </div>
        <div>
            <a href="{{ route('admin.auth.secteurs.create') }}" type="button" class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                Ajouter
            </a>
        </div>

    </div>
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg ">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-lg text-black font-bold uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="p-4">
                        <div class="flex items-center ">
                            N°
                        </div>
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Nom du secteur
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($secteurs as $secteur)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td class="w-4 p-4">
                        <div class="flex items-center">
                            {{ $loop->index + 1 }}
                        </div>
                    </td>
                    <td scope="row" class="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white">
                        <div class="pl-3">
                            <div class="text-base font-semibold">{{ $secteur->nom }}</div>
                        </div>
                    </td>
                    <td class=" px-6 py-4">
                        <form action="{{ route('admin.auth.secteurs.destroy',$secteur->id) }}" method="Post">
                            <a href="{{ route('admin.auth.secteurs.edit',$secteur->id) }}" type="button" class="bg-blue-500 text-lg text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md  py-1 px-3 text-white rounded uppercase my-4" title="Editer">
                                <i class="fa-solid fa-pen-to-square text-2xl"></i>
                            </a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="bg-red-500 text-white text-2xl hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md py-1 px-3 text-white rounded uppercase my-4" title="Supprimer">
                                <i class="fa-solid fa-trash"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="mt-5">{{ $secteurs->links() }}</div>
</div>