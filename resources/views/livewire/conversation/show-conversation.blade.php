<div class="show-conversation-container rounded-lg border shadow-md">
    <div class="border-b p-1 flex items-center">
        <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg px-3 text-white rounded uppercase my-4 mx-2" title="retourner à vos conversations">
            @switch(auth()->user()->role)
                @case('admin')    
                    <a href="{{ route('admin.auth.conversations') }}">
                            <i class="fa-solid fa-arrow-left text-2xl"></i>
                    </a>
                    @break
                @case('recruteur')
                    <a href="{{ route('recruteur.auth.conversations') }}">
                        <i class="fa-solid fa-arrow-left text-2xl"></i>
                    </a>
                    @break
                @default
                <a href="{{ route('candidat.auth.conversations') }}">
                    <i class="fa-solid fa-arrow-left text-2xl"></i>
                </a>
            @endswitch
        </button>
        <h5 class="text-bold text-xl">{{ $to_user }}</h5>
    </div>
    <ul id="show-conversation" class="show-conversation-body w-full h-full overflow-y-scroll flex flex-col bg-slate-200">
        @foreach ($conversation->messages as $message)
            @if ($message->user_id == auth()->user()->id)
                <li class="bg-green-300 w-1/2 self-end rounded my-2 p-1 mr-1">{{ $message->contenu }}</li>
            @else
                <li class="bg-white w-1/2 self-start rounded my-2 p-1 ml-1">{{ $message->contenu }}</li>
            @endif
        @endforeach
    </ul>
    <div class="bg-slate-200">
        <input 
            type="text" 
            wire:model="text_message" 
            class="rounded-full w-11/12" 
            autofocus 
            placeholder="Message"
            wire:keydown.enter.prevent="saveMessage"
        >
    </div>
</div>

@push('scripts')
    <script>

        let container = document.querySelector('#show-conversation');

        function scrollDown() { container.scrollTop = container.scrollHeight; }

        window.addEventListener('DOMContentLoaded', () => {
            scrollDown();
        });
        
        window.addEventListener('scrollDown', () => {

            Livewire.hook('message.processed', () => {
                if (container.scrollTop + container.clientHeight + 100 < container.scrollHeight) {
                    return;
                }
                scrollDown();
            })

            scrollDown();
        });
    </script>
@endpush