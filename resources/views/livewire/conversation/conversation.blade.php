<div x-data class="conversation-container conversation-container--margin grid grid-cols-1 md:grid-cols-2 w-full p-5 rounded-lg border shadow-md">
    
    @if (auth()->user()->role == 'admin' || auth()->user()->role == 'recruteur')    
        <div class="search-user">
            <div class="mb-4 p-1 flex items-center">
                <div class="inline-block relative w-full">
                    <input 
                        type="text" 
                        wire:model="text_search"
                        placeholder="Rechecher un utilisateur"
                        autofocus
                        class="rounded w-11/12 ml-2 mt-3 bg-gray-100 text-xl"
                        @click.outside="$wire.text_search = ''"
                    >
                    
                    <div 
                        @class([
                            'h-36' =>strlen($text_search) > 0 && count($users) > 0,
                            'overflow-y-scroll' =>strlen($text_search) > 0 && count($users) > 0,
                            'absolute border rounded bg-gray-100 text-md w-56 mt-1 w-full'
                        ])
                    >
                        @if (strlen($text_search) > 0)    
                                @if (count($users) > 0)    
                                    @foreach ($users as $user)
                                    <p  wire:click="$set('sendTo', {{ $user }})" class="p-2 hover:bg-green-400 hover:text-white text-green-500 cursor-pointer">{{ $user->prenom }} {{ $user->nom }}</p>
                                    @endforeach
                                @else
                                    <span class="text-orange-500 p-1">0 Résultat pour {{ $text_search }}</span>
                                @endif
                        @endif
                    </div>
                </div>
            </div>

            @if ($sendTo)
                <span class="bg-gray-200 p-2 ml-2 rounded my-3">{{ $sendTo['prenom'] }} {{ $sendTo['nom'] }}</span>
            @endif

            <input 
                type="text" 
                wire:model="text_message"
                class="rounded-full w-11/12 ml-2 mt-3" 
                autofocus 
                placeholder="Message"
                wire:keydown.enter.prevent="saveMessage"
            >
        </div>
    @endif

    <div class="mt-5 md:m-0">
        <div class="conversation-body border rounded w-full h-full overflow-y-scroll">
            @forelse ($conversations as $conversation)
                @can('isAdmin')    
                    <a href="{{ route('admin.auth.conversations.show', $conversation->id) }}">
                        <x-conversations-card.conversations :conversation="$conversation" />
                    </a>
                @elsecan('isRecruteur')
                    <a href="{{ route('recruteur.auth.conversations.show', $conversation->id) }}">
                        <x-conversations-card.conversations :conversation="$conversation" />
                    </a>
                @else
                    <a href="{{ route('candidat.auth.conversations.show', $conversation->id) }}">
                        <x-conversations-card.conversations :conversation="$conversation" />
                    </a>
                @endcan
            @empty
                <h3 class="text-center text-2xl mt-10">Vous n'avez pas de conversation.</h3>
            @endforelse
        </div>
    </div>
</div>