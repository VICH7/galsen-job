<div class="grid grid-cols-1 md:grid-cols-3 gap-6">

    <div class="md:px-2 h-64 md:h-96 md:sticky top-24">

        <div class="border rounded-lg mb-6">
            <div class="p-2 bg-gray-300"></div>
            <div class="ml-2">
                <p class="text-xl cursor-pointer" wire:click="old_or_recent">
                    
                    @if ($old)
                    <i class="fa-solid fa-arrow-down-short-wide"></i>
                        <span>Récent</span>
                        
                    @else
                    <i class="fa-solid fa-arrow-up-short-wide"></i>
                    <span>Ancient</span>
                    @endif
                </p>
            </div>
        </div>

        <div class="border rounded-lg mb-6">
            <div class="p-2 bg-gray-300"></div>
            <p>Type de poste</p>
            <div class="ml-2 flex justify-around">
               <label>
                    <input wire:model="typePosteSelect" type="checkbox" value="cdd"> CDD
               </label>
               <label>
                    <input wire:model="typePosteSelect" type="checkbox" value="cdi"> CDI
               </label>
               <label>
                    <input wire:model="typePosteSelect" type="checkbox" value="stage"> STAGE
               </label>
            </div>
        </div>

        <div class="border rounded-lg mb-6">
            <div class="p-2 bg-gray-300"></div>
            <div class="ml-2 flex flex-wrap">
                @foreach ($secteurs as $secteur)
                    <label class="mx-2">
                        <input wire:model="secteursSelect" type="checkbox" value="{{ $secteur->id }}"> {{$secteur->nom}}
                    </label>
                @endforeach
            </div>
        </div>
    </div>
    
    <div class="md:col-span-2 p-2">

        @forelse ($offres as $offre)
            <div class=" mb-1 p-2 rounded-lg border shadow-md">
                <div>
                    <div>
                        <h3 class="text-xl">{{ $offre->titre_poste }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Illuminate\Support\Str::limit($offre->description, 100) !!}
                        <p>
                            @can('isAdmin')
                                <a href="{{ route('admin.auth.offres.show', ['offre' => $offre->id]) }}" class="text-red-500">consulter</a>
                            @endcan

                            @can('isRecruteur')
                                <a href="{{ route('recruteur.auth.offres.show', ['offre' => $offre->id]) }}" class="text-red-500">consulter</a>
                            @endcan
                        </p>
                    </div>
                    <div>
                        <p class="font-medium">{{ $offre->type_poste }}</p>
                        
                        <span class="italic text-gray-700">{{ $offre->created_at->diffForHumans() }}</span>
                    </div>

                </div>
            </div>
        @empty
            <p class="text-center">Pas d'offres pour le moment</p>
        @endforelse

        <div class="mt-3">
            {{ $offres->links() }}
        </div>
    </div>
</div>
