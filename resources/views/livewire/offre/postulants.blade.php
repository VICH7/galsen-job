
<div>
    <h2 class="mt-5 font-bold mb-3 text-2xl">Liste des postulants pour cette Offre</h2>

    <div class="flex items-center justify-between py-4 bg-white dark:bg-gray-800">
        <label for="table-search" class="sr-only">Search</label>
        <div class="relative">
             <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                       <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                  </svg>
             </div>
             <input wire:model="search" type="text" id="table-search-users" class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-64 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Chercher">
        </div>
        <div>
            <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4" title="retourner à l'offre">
                <a href="{{ route('admin.auth.offres.show', ['offre' => $offre->id]) }}">
                        <i class="fa-solid fa-arrow-left text-2xl"></i>
                </a>
            </button>
        </div>

   </div>


    <div class="overflow-x-auto shadow-md sm:rounded-lg ">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-lg text-black font-bold uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                        <th scope="col" class="p-4">
                            <div class="flex items-center ">
                                N°
                            </div>
                        </th>
                        <th scope="col" class="p-4">
                            <div class="flex items-center ">
                                Nom et Prenom
                            </div>
                        </th>
                        <th scope="col" class="px-6 py-3">
                            Email
                        </th>
                        <th scope="col"  class="px-6 py-3 text-center">
                            Statut Validation
                        </th>
                        <th scope="col" colspan="3" class="px-6 py-3 text-center">
                            Action
                        </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($postulants as $postulant)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 ">
                        <td class="w-4 p-4">
                            <div>
                                {{ $postulant->id }}
                            </div>
                        </td>
                        <td class="px-6 py-4">
                            <div class="pl-3">
                                <div class="text-base font-semibold">
                                    <a 
                                        class="flex items-center" 
                                        href="{{ auth()->user()->role == "admin" ? 
                                            route('admin.auth.profils.userProfil', ['user' => $postulant->id]) : 
                                            route('recruteur.auth.profils.userProfil', ['user' => $postulant->id]) 
                                        }}"
                                    >
                                        @if ($postulant->image)
                                            <img class="mx-3" src="{{ Storage::url($postulant->image) }}" alt="profil par défaut" style="width:30px; height:30px; border-radius:50%;">
                                        @endif
                                        {{ $postulant->nom }} {{ $postulant->prenom }}
                                    </a>
                                </div>
                            </div>
                        </td>
                        <td class=" px-6 py-4">
                            <div class="pl-3">
                                <div class="text-base font-semibold">{{ $postulant->email }}</div>
                            </div>
                        </td>
                        <td class=" px-6 py-4">
                            <div class="pl-3">
                                <div class="text-base font-semibold text-center">
                                    @if ($postulant->pivot->validated == 'accept')
                                            <i class="fa-solid fa-circle-check text-green-500 text-xl" title="accepté"></i>
                                    @elseif($postulant->pivot->validated == 'pending')
                                        <i class="fa-solid fa-ellipsis text-gray-500 text-xl" title="en attente"></i>
                                    @else
                                            <i class="fa-solid fa-circle-xmark text-red-500 text-xl" title="rejeté"></i>
                                    @endif
                                </div>
                            </div>
                        </td>
                        <td class=" px-6 py-4">
                            <button wire:click="action('accept', {{ $postulant->id }})" class="bg-blue-500 font-semibold hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                                Accepter
                            </button>
                        </td>
                        <td class=" px-6 py-4">
                            <button wire:click="action('reject', {{ $postulant->id }})" class="bg-red-500 font-semibold hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                                Rejeter
                            </button>
                        </td>
                        <td class="px-6 py-4">
                            <button wire:click="action('pending', {{ $postulant->id }})" class="bg-orange-500 font-semibold hover:bg-orange-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
                                En attente
                            </button>
                        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="mt-3">{{ $postulants->links() }}</div>
</div>