<div>
    <div class="mb-4">
        <h5 class="text-center font-semibold text-xl mb-1">Secteurs</h5>
        <div class="ml-2 flex flex-wrap">
            @foreach ($secteurs as $secteur)
                <label class="mx-2">
                    <input wire:model="secteursSelect" type="checkbox" value="{{ $secteur->id }}"> {{$secteur->nom}}
                </label>
            @endforeach
        </div>
    </div>

    <div class="mb-4">
        <h5 class="text-center font-semibold text-xl mb-1">Type de poste</h5>
        <div class="ml-2 flex justify-around">
            <label>
                <input wire:model="typePosteSelect" type="checkbox" value="cdd"> CDD
            </label>
            <label>
                <input wire:model="typePosteSelect" type="checkbox" value="cdi"> CDI
            </label>
            <label>
                <input wire:model="typePosteSelect" type="checkbox" value="stage"> STAGE
            </label>
         </div>
    </div>
    

    <div class="py-4">
        <label for="table-search" class="sr-only">Search</label>
        <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                    </svg>
                </div>
                <input 
                    wire:model="search" 
                    type="text" 
                    id="table-search-users" 
                    class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-64 bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
                    placeholder="Rechercher par poste">
        </div>
    </div>

    @if (session('success'))
        <p class="text-green-500 mb-4 text-center">{{ session('success') }}</p>
    @endif

    <div class="overflow-x-auto shadow-md sm:rounded-lg ">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
             <thead class="text-lg text-black font-bold uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                  <tr>
                            <th scope="col" class="p-4">
                            <div class="flex items-center ">
                                 N°
                            </div>
                            </th>
                            <th scope="col" class="p-4">
                            <div class="flex items-center ">
                                 Poste
                            </div>
                            </th>
                            <th scope="col" class="px-6 py-3">
                                 type de poste
                            </th>
                            <th scope="col"  class="px-6 py-3 text-center">
                            Statut Validation
                            </th>
                            <th scope="col" colspan="3" class="px-6 py-3 text-center">
                            Action
                            </th>
                  </tr>
             </thead>
             <tbody>
                  @foreach ($offres as $offre)
                  <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600 ">
                            <td class="w-4 p-4">
                                 <div>
                                      {{ $offre->id }}
                                 </div>
                            </td>
                            <td class="px-6 py-4">
                                 <div class="pl-3">
                                      <div class="text-base font-semibold">{{ $offre->titre_poste }}</div>
                                 </div>
                            </td>
                            <td class="px-6 py-4">
                                 <div class="pl-3">
                                      <div class="text-base font-semibold">{{ $offre->type_poste }}</div>
                                 </div>
                            </td>
                            <td class="px-6 py-4 text-center">
                                 @if ($offre->pivot->validated == 'accept')
                                      <i class="fa-solid fa-circle-check text-green-500 text-xl" title="accepté"></i>
                                 @elseif($offre->pivot->validated == 'pending')
                                      <i class="fa-solid fa-ellipsis text-gray-500 text-xl" title="en attente"></i>
                                 @else
                                      <i class="fa-solid fa-circle-xmark text-red-500 text-xl" title="rejeté"></i>
                                 @endif
                            </td>
                            <td class="px-6 py-4 text-center">
                                 <a href="{{ route('candidat.auth.offres.show', ['offre' => $offre->id]) }}" class="bg-blue-500 p-1 text-white rounded uppercase text-xl">
                                      <i class="fa-solid fa-eye" title="Voir plus"></i>
                                 </a>
                            </td>
                            <td class="px-6 py-4 text-center">
                                 <a href="{{ route('candidat.auth.offres.cancel', ['offre' => $offre->id]) }}" class="bg-red-500 p-1 text-white rounded uppercase text-md">
                                      Annuler
                                 </a>
                            </td>
                  </tr>
                  @endforeach
             </tbody>
        </table>
   </div>
   <div class="mt-3">{{ $offres->links() }}</div>
</div>
