<div class="grid grid-cols-1 md:grid-cols-3 gap-6">

    <div class="md:px-2 h-64 md:h-96 md:sticky top-24">

        <div class="border rounded-lg mb-6">
            <div class="p-2 bg-gray-300"></div>
            <div class="ml-2">
                <p class="text-xl cursor-pointer" wire:click="old_or_recent">
                    
                    @if ($old)
                    <i class="fa-solid fa-arrow-down-short-wide"></i>
                        <span>Récent</span>
                        
                    @else
                    <i class="fa-solid fa-arrow-up-short-wide"></i>
                    <span>Ancient</span>
                    @endif
                </p>
            </div>
        </div>

        <div class="border rounded-lg mb-6">
            <div class="p-2 bg-gray-300"></div>
            <p>Type de poste</p>
            <div class="ml-2 flex justify-around">
               <label>
                    <input wire:model="typePosteSelect" type="checkbox" value="cdd"> CDD
               </label>
               <label>
                    <input wire:model="typePosteSelect" type="checkbox" value="cdi"> CDI
               </label>
               <label>
                    <input wire:model="typePosteSelect" type="checkbox" value="stage"> STAGE
               </label>
            </div>
        </div>

        <div class="border rounded-lg mb-6">
            <div class="p-2 bg-gray-300"></div>
            <div class="ml-2 flex flex-wrap">
                @foreach ($secteurs as $secteur)
                    <label class="mx-2">
                        <input wire:model="secteursSelect" type="checkbox" value="{{ $secteur->id }}"> {{$secteur->nom}}
                    </label>
                @endforeach
            </div>
        </div>
    </div>
    
    <div class="md:col-span-2 p-2">

        @forelse ($offres as $offre)
            <div class=" mb-1 flex justify-between items-center p-2 rounded-lg shadow-lg">
                <div>
                    <div>
                        <h3 class="text-xl">{{ $offre->titre_poste }}</h3>
                    </div>
                    <div class="card-body">
                        {!! Illuminate\Support\Str::limit($offre->description, 100) !!}
                        @can('isAdmin')
                            <p>
                                <a href="{{ route('admin.auth.offres.show', ['offre' => $offre->id]) }}" class="text-red-500">consulter</a>
                            </p>
                        @elsecan('isRecruteur')
                            <p>
                                <a href="{{ route('recruteur.auth.offres.show', ['offre' => $offre->id]) }}" class="text-red-500">consulter</a>
                            </p>
                        @elsecan('isCandidat')    
                            <p>
                                <a href="{{ route('candidat.auth.offres.show', ['offre' => $offre->id]) }}" class="text-red-500">consulter</a>
                            </p>
                        @else
                        <p>
                            <a href="{{ route('guest-offers.show', ['offre' => $offre->id]) }}" class="text-red-500">consulter</a>
                        </p>
                        @endcan
                    </div>
                    <div>
                        <p class="font-medium">{{ $offre->type_poste }}</p>
                        
                        <span class="italic text-gray-700">{{ $offre->created_at->diffForHumans() }}</span>
                    </div>

                </div>

                <button class="h-5 w-5 text-gray-600 focus:outline-none" wire:click="addLike({{ $offre }})">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="{{ $offre->isliked() ? 'red' : 'white' }}" viewBox="0 0 24 24" stroke-width="1.5" stroke="red" class="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M21 8.25c0-2.485-2.099-4.5-4.688-4.5-1.935 0-3.597 1.126-4.312 2.733-.715-1.607-2.377-2.733-4.313-2.733C5.1 3.75 3 5.765 3 8.25c0 7.22 9 12 9 12s9-4.78 9-12z" />
                    </svg>
                </button>
            </div>
        @empty
            <p class="text-center">Pas d'offres pour le moment</p>
        @endforelse

        <div class="mt-3">
            {{ $offres->links() }}
        </div>
    </div>
</div>
