<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
     <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <title>Laravel</title>

          <!-- Fonts -->
          <link rel="preconnect" href="https://fonts.bunny.net">
          <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />


          @vite(['resources/css/app.css', 'resources/js/app.js'])
          <style>
               [x-cloak] { display: none !important; }
          </style>
          @livewireStyles       
     </head>
     <body class="antialiased">

          @include('partials.navbar-guest')
          
          <div class="container-guest-offer">
               <h1 class="mt-5 text-center underline mb-3 text-2xl">Les Offres</h1>
     
               @livewire('offre.offre', ['secteurs' => $secteurs ])
          </div>

          @include('partials.admin-footer')

          @livewireScripts
     </body>
</html>
