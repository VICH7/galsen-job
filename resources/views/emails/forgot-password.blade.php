@extends('layout-main.email-main')

@section('content')
    <p class="mb-3 font-semibold text-center">
     Salutation, vous avez fait une demande pour réinitialiser votre mot de passe.
     <br>
     Pour Réinitialiser Votre Mot de passe Veuillez cliquer le lien ci-dessous.
    </p>

    <a href="{{ route('password.reset', ['token' => $token]) }}">Réinitialiser mon mot de passe</a>
@endsection