@extends('layout-main.auth-main')

@section('content')
<div>
    <div name="header">
          <h2 class="font-semibold text-center text-xl pt-3 text-gray-800 dark:text-gray-200 leading-tight">
               {{ __('Section Paramètre') }}
          </h2>
     </div>
    

     <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('admin.parametre.partials.update-password-form')
                </div>
            </div>

            <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('admin.parametre.partials.delete-user-form')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection