@extends('layout-main.auth-main')

@section('content')


<div class="container-auth">
     @if ($message = Session::get('success'))
     <div id="alert-border-3" class="flex p-4 mb-4 text-green-800 border-t-4 border-green-300 bg-green-50 dark:text-green-400 dark:bg-gray-800 dark:border-green-800" role="alert">
          <svg class="flex-shrink-0 w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
               <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
          </svg>
          <div class="ml-3 text-sm font-medium">
               {{ $message }}
          </div>
     </div>


     @endif

     <div class="w-10/12 box-border mx-auto p-6 border border-gray-200 rounded">
          <div class="flex items-center flex-wrap py-2">
               <div class="px-9 justify-self-center">
                    @if (auth()->user()->image)
                         <img src="{{ Storage::url(auth()->user()->image) }}" alt="profil par défaut" style="width:100px; height:100px; border-radius:50%">     
                    @else
                         <img src="/avatars/default.png" alt="profil par défaut" style="width:100px; height:100px; border-radius:50%">
                    @endif             
               </div>

               <p class="text-3xl px-4 font-bold mb-2 capitalize mt-5 md:mt-0 "><i class="fa-solid fa-user-tie"></i> {{ auth()->user()->nom }} {{ auth()->user()->prenom }}</p>
          </div>

          <hr class="my-4">

          <div class="flex items-center flex-wrap py-2">
               <p class="text-xl mb-2 px-4  md:border-r"><i class="fa-solid fa-location-dot"></i> {{ auth()->user()->adresse }}</p>
               <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-envelope"></i> {{ auth()->user()->email }}</p>
          </div>

          <hr class="my-4">

          <button class="cursor-pointeruppercase bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded uppercase my-4 ml-3">
               {{-- <a href="{{ route('admin.auth.parametres') }}">Modifier</a> --}}
               <a href="{{ route('admin.auth.profils.edit') }}">Modifier</a>
           </button>

          {{-- <livewire:profil.show-edit-profil-modal /> --}}
     </div>
     
     {{-- @livewire('profil.profil', ['user' => auth()->user()]) --}}
</div>
@endsection