@extends('layout-main.auth-main')

@section('content')

<div class="container-auth container-auth--margin">

     <h1 class="text-2xl font-bold">Mise à jour du secteur</h1>
     @if(session('status'))
     <div class="alert alert-success mb-1 mt-1">
          {{ session('status') }}
     </div>
     @endif

     <form class="mt-5 w-1/2" action="{{ route('admin.auth.secteurs.update',$secteur->id) }}" method="POST">
          @csrf
          @method('PUT')
          <div class="relative z-0 w-full mb-6 group">
               <input type="text" name="nom" value="{{ $secteur->nom }}" id="nom" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
               <label for="nom" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                    Nom du secteur
               </label>
               @error('nom')
               <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
               @enderror
          </div>
          <button type="submit" class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">Modifier</button>
          <button class="bg-red-500 text-white hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
               <a class="" href="{{ route('admin.auth.secteurs') }}" enctype="multipart/form-data">
                    Annuler
               </a>
          </button>
     </form>

</div>
@endsection