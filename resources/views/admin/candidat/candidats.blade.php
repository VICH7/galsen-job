@extends('layout-main.auth-main')

@section('content')

<div class="container-auth container-auth--margin">

     <h1 class="text-2xl mb-2 font-bold">La listes de tous les candidats</h1>

     @livewire('users-management.candidat')
</div>
@endsection
