@extends('layout-main.auth-main')

@section('content')

<div class="container-auth container-auth--margin">
     @if ($message = Session::get('success'))
     <div id="alert-border-3" class="flex p-4 mb-4 text-green-800 border-t-4 border-green-300 bg-green-50 dark:text-green-400 dark:bg-gray-800 dark:border-green-800" role="alert">
          <svg class="flex-shrink-0 w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
               <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path>
          </svg>
          <div class="ml-3 text-sm font-medium">
               {{ $message }}
          </div>
     </div>
     @endif

     @include('partials.add-offer-button')
     <h1 class="mt-5 font-bold mb-3 text-2xl">{{ $offre->titre_poste }}</h1>
     <div class="flex items-center mb-20">
          <div class="w-28 h-28">
               @if (optional($offre->user->infoRecruteur)->logo_entreprise)
               <img class="w-full h-full" src="{{ $offre->user->infoRecruteur->logo_entreprise }}" alt="logo de l'entreprise">
               @else
               <img class="w-full h-full" src="/logos/default.avif" alt="logo par defaut">
               @endif
          </div>
          <div class="ml-2">
               <p class="capitalize"><i class="fa-solid fa-location-dot mr-1 text-gray-500"></i>{{ $offre->localisation_poste }}</p>
               <p class="capitalize">
                    @foreach ($offre->secteurs as $secteur)
                    {{ $secteur->nom }},&nbsp;
                    @endforeach
               </p>
               <p class="uppercase">{{ $offre->type_poste }}</p>
          </div>
     </div>

     <div class="mb-5">
          <hr>
          @if ($offre->site_candidature)
          <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded uppercase my-4">
               <a href="{{ $offre->site_candidature }}">Postuler</a>
          </button>
          @else
          <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded uppercase my-4">Postuler</button>
          @endif
          <hr>
     </div>
     @if (optional($offre->user->infoRecruteur)->description_entreprise)
     <h2 class="text-1xl font-bold">Description entreprise</h2>
     {!! optional($offre->user->infoRecruteur)->description_entreprise !!}
     <hr class="my-4">
     @endif

     {!! $offre->description !!}

     @if ($offre->salaire)
     <p class="mt-5">Salaire: {{ $offre->salaire }} FCFA</p>
     @endif



     @can('recruteur-admin-model', $offre)
     <div class="flex justify-around">

          @can('mofify', $offre)
          <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4" title="éditer">
               <a href="{{ route('admin.auth.offres.edit', ['offre' => $offre->id]) }}">
                    <i class="fa-solid fa-pen-to-square text-2xl"></i>
               </a>
          </button>
          @endcan

          <button class="bg-green-500 text-white hover:bg-green-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4" title="voir les postulant de cette offre">
               <a href="{{ route('admin.auth.offres.postulants', ['offre' => $offre->id]) }}">
                    <i class="fa-solid fa-handshake text-2xl"></i>
               </a>
          </button>

          <form action="{{ route('admin.auth.offres.destroy', ['offre' => $offre->id]) }}" method="post">
               @csrf
               @method('DELETE')
               <button class="bg-red-500 text-white hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4" title="supprimer">
                    <i class="fa-solid fa-trash"></i>
               </button>
          </form>
     </div>
     @endcan
</div>

@endsection