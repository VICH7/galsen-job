@extends('layout-main.auth-main')

@section('content')

<div class="container-auth container-auth--margin">

     @include('partials.add-offer-button')
     <h1 class="mt-5 font-bold mb-3 text-2xl">{{ $offre->titre_poste }}</h1>

     <div class="flex items-center mb-20">
          <div class="w-28 h-28">
               @if (optional($offre->user->infoRecruteur)->logo_entreprise)
                    <img class="w-full h-full" src="{{ $offre->user->infoRecruteur->logo_entreprise }}" alt="logo de l'entreprise">
               @else
                    <img class="w-full h-full" src="/logos/default.avif" alt="logo par defaut">
               @endif
          </div>
          <div class="ml-2">
               <p class="capitalize"><i class="fa-solid fa-location-dot mr-1 text-gray-500"></i>{{ $offre->localisation_poste }}</p>
               <p class="capitalize">
                    @foreach ($offre->secteurs as $secteur)
                         {{ $secteur->nom }},&nbsp;
                    @endforeach
               </p>
               <p class="uppercase">{{ $offre->type_poste }}</p>
          </div>
     </div>
     
     @livewire('offre.postulants', ['offre' => $offre])
</div>
@endsection
