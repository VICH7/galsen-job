@extends('layout-main.auth-main')

@section('content')
<div>
     @if ($message = Session::get('success'))
          <div class="text-green-600 font-bold text-center my-2 text-xl">
               <p>{{ $message }}</p>
          </div>
     @endif

     <div class="w-10/12 box-border mb-4 mx-auto p-6 border border-gray-200 rounded">
          <h2 class="text-center text-gray-500 font-bold text-4xl">Information Personnelle</h2>
          <hr class="my-4">
          <div class="flex items-center flex-wrap py-2">
               <div class="px-9 justify-self-center">
                    @if ($user->image)
                         <img src="{{ Storage::url($user->image) }}" alt="profil par défaut" style="width:100px; height:100px; border-radius:50%">     
                    @else
                         <img src="/avatars/default.png" alt="profil par défaut" style="width:100px; height:100px; border-radius:50%">
                    @endif             
               </div>

               <p class="text-3xl px-4 font-bold mb-2 capitalize mt-5 md:mt-0 "><i class="fa-solid fa-user-tie"></i> {{ $user->nom }} {{ $user->prenom }}</p>
          </div>
          
          <hr class="my-4">
          
          <div class="flex items-center flex-wrap py-2">
               <p class="text-xl mb-2 px-4  md:border-r"><i class="fa-solid fa-location-dot"></i> {{ $user->adresse }}</p>
               <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-envelope"></i> {{ $user->email }}</p>
               <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-phone"></i> {{ $user->telephone }}</p>
          </div>

     </div>

     <div class="w-10/12 box-border mx-auto p-6 border border-gray-200 rounded">
          <h2 class="text-center text-gray-500 font-bold text-4xl">Information Entreprise</h2>

          <hr class="my-4">

          <div class="flex items-center flex-wrap py-2">
               <div class="px-9 justify-self-center">
                    @if ($user->infoRecruteur->logo_entreprise)
                         <img src="{{ Storage::url($user->infoRecruteur->logo_entreprise) }}" alt="logo de l'entreprise" style="width:100px; height:100px; border-radius:50%">
                    @endif            
               </div>

               <p class="text-3xl px-4 font-bold mb-2 capitalize mt-5 md:mt-0 "> {{ $user->infoRecruteur->nom_entreprise }}</p>
          </div>
          
          <hr class="my-4">
          
          @if ($user->infoRecruteur->description_entreprise)    
               <div class=" py-2">
                    {!! $user->infoRecruteur->description_entreprise !!}
               </div>

               <hr class="my-4">
          @endif

     </div>
</div>
@endsection