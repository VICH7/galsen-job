@extends('layout-main.auth-main')

@section('content')

<div>
     @if ($message = Session::get('success'))
          <div class="text-green-600 font-bold text-center my-2 text-xl">
               <p>{{ $message }}</p>
          </div>
     @endif

     <div class="w-10/12 box-border mb-4 mx-auto p-6 border border-gray-200 rounded">
          <h2 class="text-center text-gray-500 font-bold text-4xl">Information Personnelle</h2>
          <div class="flex items-center flex-wrap py-2">
               <div class="px-9 justify-self-center">
                    @if ($user->image)
                         <img src="{{ Storage::url($user->image) }}" alt="Image profil" style="width:100px; height:100px; border-radius:50%">     
                    @else
                         <img src="/avatars/default.png" alt="profil par défaut" style="width:100px; height:100px; border-radius:50%">
                    @endif             
               </div>

               <p class="text-3xl px-4 font-bold mb-2 capitalize mt-5 md:mt-0 "><i class="fa-solid fa-user-tie"></i> {{ $user->nom }} {{ $user->prenom }}</p>
          </div>
          
          <hr class="my-4">
          
          <div class="flex items-center flex-wrap py-2">
               <p class="text-xl mb-2 px-4  md:border-r"><i class="fa-solid fa-location-dot"></i> {{ $user->adresse }}</p>
               <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-envelope"></i> {{ $user->email }}</p>
               <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-phone"></i> {{ $user->telephone }}</p>
          </div>

     </div>

     <div class="w-10/12 box-border mb-4 mx-auto p-6 border border-gray-200 rounded">
          <h2 class="text-center text-gray-500 font-bold text-4xl">Information Supplémentaire</h2>
          
          <hr class="my-4">

          @if ($user->infoCandidat->cv)
               <h2 class="font-bold text-xl">Cv</h2>
               @if (
                    pathinfo($user->infoCandidat->cv, PATHINFO_EXTENSION) == 'jpg' ||
                    pathinfo($user->infoCandidat->cv, PATHINFO_EXTENSION) == 'png' ||
                    pathinfo($user->infoCandidat->cv, PATHINFO_EXTENSION) == 'jpeg'
               )
                    <div class="py-2">
                         <img src="{{ Storage::url($user->infoCandidat->cv) }}" alt="image cv" style="width:200px; height:200px;">
                    </div>
               @else
                    <div class="py-2 flex items-center">
                         <i class="fa-solid fa-book mt-1 mr-1"></i> Votre CV
                         <a class="ml-1" href="{{ Storage::url($user->infoCandidat->cv) }}"><i class="fa-solid fa-download text-blue-500 cursor-pointer hover:text-blue-700"></i></a>   
                    </div>     
               @endif
               <hr class="my-4">
          @endif

          @if (
               $user->infoCandidat->formation ||
               $user->infoCandidat->profession ||
               $user->infoCandidat->langue
          )
              
              <div class="flex items-center flex-wrap py-2">
                    @if ($user->infoCandidat->formation)
                         <p class="text-xl mb-2 px-4  md:border-r"><i class="fa-solid fa-building-columns"></i> {{ $user->infoCandidat->formation }}</p>
                    @endif

                    @if ($user->infoCandidat->profession)
                         <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-table"></i> {{ $user->infoCandidat->profession }}</p>
                    @endif

                    @if ($user->infoCandidat->langue)
                         <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-language"></i> {{ $user->infoCandidat->langue }}</p>
                    @endif
              </div>
              
              <hr class="my-4">
          @endif

          @if (
               $user->infoCandidat->niveau_etude ||
               $user->infoCandidat->competence
          )
              
              <div class="flex items-center flex-wrap py-2">
                    @if ($user->infoCandidat->niveau_etude)
                         <p class="text-xl mb-2 px-4  md:border-r"><i class="fa-solid fa-turn-up"></i> {{ $user->infoCandidat->niveau_etude }}</p>
                    @endif

                    @if ($user->infoCandidat->competence)
                    <p class="text-xl mb-2 px-4 md:border-l"><i class="fa-solid fa-kitchen-set"></i></i> {{ $user->infoCandidat->competence }}</p>
                    @endif
              </div>
              
              <hr class="my-4">
          @endif


          @if ($user->infoCandidat->motivation)
               <div class="py-2">
                    <h2 class="font-bold text-xl">Motivation</h2>
                    {!! $user->infoCandidat->motivation !!}
               </div>
               <hr class="my-4">
          @else
               <div class="py-2">
                    <p>Motivations pas encore rédigées</p>
               </div>
               <hr class="my-4">
          @endif


          @if ($user->infoCandidat->experience)
               <div class="py-2">
                    <h2 class="font-bold text-xl">Expérience</h2>
                    {!! $user->infoCandidat->experience !!}
               </div>
               <hr class="my-4">
          @else
               <div class="py-2">
                    <p>Expériences pas encore rédigées</p>
               </div>
               <hr class="my-4">
          @endif

     </div>

</div>
@endsection