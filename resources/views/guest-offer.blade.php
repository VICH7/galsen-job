<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
     <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <title>Laravel</title>

          <!-- Fonts -->
          <link rel="preconnect" href="https://fonts.bunny.net">
          <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />


          @vite(['resources/css/app.css', 'resources/js/app.js'])
          <style>
               [x-cloak] { display: none !important; }
          </style>
          @livewireStyles       
     </head>
     <body class="antialiased">

          @include('partials.navbar-guest')
          
          <div class="container-guest-offer" x-data="{open: false}">
               <div x-cloak x-show="open" x-transition.opacity.duration.500ms  @flash-message.window="open = true; setTimeout(() => open=false,5000)">
                    <div   class="bg-orange-500 px-1 py-2 rounded fixed end-0 z-10 opacity-90 mr-3">
                        Merci de vous connecter pour Postuler !
                    </div>
                </div>
               <x-offres.show-offer :offre="$offre" />
          </div>

          @include('partials.admin-footer')

          @livewireScripts
     </body>
</html>
