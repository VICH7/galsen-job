@extends('layout-main.auth-main')

@section('content')

<div class="container-auth container-auth-candidat--margin ">
     <h1 class="text-semi-bold text-center text-2xl">Vos Conversations</h1>
     @livewire('conversation.conversation', ['conversations' => $conversations])
</div>
@endsection
