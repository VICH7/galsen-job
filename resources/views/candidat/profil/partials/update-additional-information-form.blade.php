<section>

     <header>
          <h2 class="text-xl font-bold" id="additional-information">
               {{ __("Informations Supplémentaires") }}
           </h2>
     </header>
 
     <form class="mt-5" action="{{ route('candidat.auth.profils.updateAdditional') }}" method="POST" enctype="multipart/form-data">
         @csrf
         @method('patch')

         <div class="mb-6">
               <label for="cv" class="block font-semibold text-gray-700 mb-2">CV<span class="text-sm font-normal">(Image: jpeg, png, jpg, doc, docx, pdf)</span>:</label>
               <input type="file" name="cv" id="cv" class="form-control">
               @error('cv')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>
 
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
              <div class="relative z-0 w-full mb-1">
                  <input type="text" name="profession" value="{{ auth()->user()->infoCandidat->profession }}" id="profession" class="block py-2.5 px-0 w-full text-md text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"  />
                  <label for="profession" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                      Profession
                  </label>
                  @error('profession')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                  @enderror
              </div>

              <div class="relative z-0 w-full mb-1">
                  <input type="text" name="formation" value="{{ auth()->user()->infoCandidat->formation }}" id="formation" class="block py-2.5 px-0 w-full text-md text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"  />
                  <label for="formation" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                      Formation
                  </label>
                  @error('formation')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                  @enderror
              </div>
         </div>

         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
              <div class="relative z-0 w-full mb-1">
                  <input type="text" name="langue" value="{{ auth()->user()->infoCandidat->langue }}" id="langue" class="block py-2.5 px-0 w-full text-md text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"  />
                  <label for="langue" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                      Langue
                  </label>
                  @error('langue')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                  @enderror
              </div>

              <div class="relative z-0 w-full mb-1">
                  <input type="text" name="niveau_etude" value="{{ auth()->user()->infoCandidat->niveau_etude }}" id="niveau_etude" class="block py-2.5 px-0 w-full text-md text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"  />
                  <label for="niveau_etude" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                      Niveau d'étude
                  </label>
                  @error('niveau_etude')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                  @enderror
              </div>
         </div>

         <div class="mb-4">
               <div class="relative z-0 w-full mb-1">
                    <input type="text" name="competence" value="{{ auth()->user()->infoCandidat->competence }}" id="competence" class="block py-2.5 px-0 w-full text-md text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer"  />
                    <label for="competence" class="peer-focus:font-medium absolute text-md text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">
                         Competence
                    </label>
                    @error('competence')
                         <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
               </div>
         </div> 

         <div class="mb-4">
               <label for="motivation" class="block font-semibold text-gray-700 mb-2">Description de Vos motivations</label>
               <textarea 
                    name="motivation" 
                    id="motivation" 
                    class="shadow border rounded w-full text-xl" cols="30" rows="10"
               >
                    {!! old('motivation', auth()->user()->infoCandidat->motivation) !!}
               </textarea>
               @error('motivation')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>

         <div class="mb-4">
               <label for="experience" class="block font-semibold text-gray-700 mb-2">Description de Vos expériences</label>
               <textarea 
                    name="experience" 
                    id="experience" 
                    class="shadow border rounded w-full text-xl" cols="30" rows="10"
               >
                    {!! old('experience', auth()->user()->infoCandidat->experience) !!}
               </textarea>
               @error('experience')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>
 
         <button type="submit" class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">Modifier</button>
         <button class="bg-red-500 text-white hover:bg-red-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg py-1 px-3 text-white rounded uppercase my-4">
             <a href="{{ route('recruteur.auth.profils') }}" enctype="multipart/form-data">
                 Annuler
             </a>
         </button>
 
     </form>
    
 </section>

 @push('scripts')
 <script src="https://cdn.ckeditor.com/ckeditor5/37.1.0/classic/ckeditor.js"></script>
 <script>
     ClassicEditor
         .create( document.querySelector( '#motivation' ) )
         .catch( error => {
             console.error( error );
     } );

     ClassicEditor
         .create( document.querySelector( '#experience' ) )
         .catch( error => {
             console.error( error );
     } );
 </script>
@endpush