@extends('layout-main.auth-main')

@section('content')

<div class="container-auth  py-3">

     <h1 class="text-2xl font-bold">Ajouter un cv</h1>
     @if(session('status'))
     <div class="alert alert-success mb-1 mt-1">
          {{ session('status') }}
     </div>
     @endif

     <form class="mt-5 w-1/2" action="{{ route('candidat.auth.cvs.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="mb-6">
               <label for="image">
                    Image CV <span class="text-sm font-normal">(Image: jpeg, png, jpg)</span> <span title="obligatoire" class="text-red-500">*</span> :
               </label>
               <input type="file" name="image" value="{{ old('image') }}" id="image" required />
               @error('image')
                    <div class="text-red-500 mt-1 mb-1">{{ $message }}</div>
               @enderror
          </div>
          <div class="mb-6">
               <label for="exemple_cv">
                    Fichier CV <span class="text-sm font-normal">(Fichier: doc, docx, pdf)</span> <span title="obligatoire" class="text-red-500">*</span> : <span title="obligatoire" class="text-red-500">*</span> :
               </label>
               <input type="file" name="exemple_cv" value="{{ old('exemple_cv') }}" id="exemple_cv" required />
               @error('exemple_cv')
                    <div class="text-red-500 mt-1 mb-1">{{ $message }}</div>
               @enderror
          </div>
          <button type="submit" class="uppercase text-white bg-blue-600 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Ajouter</button>
          <button class="uppercase text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
               <a href="{{ route('candidat.auth.cvs') }}">Annuler</a>
          </button>
     </form>
</div>
@endsection