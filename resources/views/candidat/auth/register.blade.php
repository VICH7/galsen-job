@extends('layout-main.guest-main')

@section('content')
<div class="container-guest container-guest--margin container-guest--max-width mx-auto rounded-lg border shadow-md p-5">
     <h1 class="text-3xl text-blue-500 mb-6 text-center">candidat Inscription</h1>
     <form method="POST" action="{{ route('candidat.store.register') }}" enctype="multipart/form-data">
         @csrf
     
         @if (session('fail'))
               <div class="bg-red-400 p-3 text-center text-white text-xl mb-3">
                    {{ session('fail') }}
               </div>
         @endif
         
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
              <!-- nom -->
               <div>
                    <label for="nom" class="block font-semibold text-gray-700 mb-2">Nom <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="nom" type="text" name="nom" 
                        class="shadow border rounded w-full text-xl @error('nom') invalid @enderror" 
                        value="{{ old('nom') }}" 
                        autocomplete="nom" 
                        placeholder="Votre nom" autofocus
                    >
                    @error('email')
                        <span class="text-red-400 text-md">
                            <span>{{ $message }}</span>
                        </span>
                    @enderror
               </div>
               <!-- Prenom -->
               <div>
                    <label for="prenom" class="block font-semibold text-gray-700 mb-2">Prenom <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="prenom" type="text" name="prenom" 
                        class="shadow border rounded w-full text-xl @error('prenom') invalid @enderror" 
                        value="{{ old('prenom') }}" 
                        autocomplete="prenom" 
                        placeholder="Votre prenom" autofocus
                    >
                    @error('prenom')
                        <span class="text-red-400 text-md">
                            <span>{{ $message }}</span>
                        </span>
                    @enderror
               </div>
         </div>
     
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
              <!-- telephone -->
              <div>
                  <label for="telephone" class="block font-semibold text-gray-700 mb-2">telephone</label>
                  <input 
                      id="telephone" type="number" name="telephone" 
                      class="shadow border rounded w-full text-xl @error('telephone') invalid @enderror" 
                      value="{{ old('telephone') }}" 
                      autocomplete="telephone" 
                      placeholder="Votre telephone" autofocus
                  >
                  @error('telephone')
                      <span class="text-red-400 text-md">
                          <span>{{ $message }}</span>
                      </span>
                  @enderror
              </div>
              <!-- adresse -->
              <div>
                   <label for="adresse" class="block font-semibold text-gray-700 mb-2">adresse</label>
                   <input 
                       id="adresse" type="text" name="adresse" 
                       class="shadow border rounded w-full text-xl @error('adresse') invalid @enderror" 
                       value="{{ old('adresse') }}" 
                       autocomplete="adresse" 
                       placeholder="Votre adresse" autofocus
                   >
                   @error('adresse')
                       <span class="text-red-400 text-md">
                           <span>{{ $message }}</span>
                       </span>
                   @enderror
              </div>
         </div>
     
         
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- ville -->
               <div>
                    <label for="ville" class="block font-semibold text-gray-700 mb-2">ville <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                         id="ville" type="text" name="ville" 
                         class="shadow border rounded w-full text-xl @error('ville') invalid @enderror" 
                         value="{{ old('ville') }}" 
                         autocomplete="ville" 
                         placeholder="Votre ville" autofocus
                    >
                    @error('ville')
                         <span class="text-red-400 text-md">
                              <span>{{ $message }}</span>
                         </span>
                    @enderror
               </div>

               <!-- email -->
               <div>
                    <label for="email" class="block font-semibold text-gray-700 mb-2">Email <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="email" type="email" name="email" 
                        class="shadow border rounded w-full text-xl @error('email') invalid @enderror" 
                        value="{{ old('email') }}" 
                        autocomplete="email" 
                        placeholder="Votre email" autofocus
                    >
                    @error('email')
                        <span class="text-red-400 text-md">
                            <span>{{ $message }}</span>
                        </span>
                    @enderror
               </div>
         </div>
     
     
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- Mot de passe -->
               <div>
                    <label for="password" class="block font-semibold text-gray-700 mb-2">Mot de passe <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="password" type="password" name="password" 
                        class="shadow border rounded w-full text-xl @error('password') invalid @enderror" 
                        value="{{ old('password') }}" 
                        autocomplete="password" 
                        placeholder="Votre mot de passe" 
                        autofocus
                    >
                   @error('password')
                        <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
               <!-- Confirmation Mot de passe -->
               <div>
                    <label for="password" class="block font-semibold text-gray-700 mb-2">Confirmation Mot de passe <span title="obligatoire" class="text-red-500">*</span></label>
                    <input 
                        id="password" type="password" name="password_confirmation" 
                        class="shadow border rounded w-full text-xl @error('password') invalid @enderror" 
                        value="{{ old('password_confirmation') }}" 
                        autocomplete="password_confirmation" 
                        placeholder="Votre mot de passe" 
                        autofocus
                    >
                   @error('password_confirmation')
                        <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
         </div>

         <br><hr><br>
         
         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- Profession -->
               <div>
                    <label for="profession" class="block font-semibold text-gray-700 mb-2"> votre Profession</label>
                    <input 
                         id="profession" type="text" name="profession" 
                         class="shadow border rounded w-full text-xl @error('profession') invalid @enderror" 
                         value="{{ old('profession') }}" 
                         autocomplete="profession" 
                         placeholder="Profession" 
                         autofocus
                    >
                    @error('profession')
                         <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>

               <!-- formation -->
               <div>
                    <label for="formation" class="block font-semibold text-gray-700 mb-2"> votre Formation</label>
                    <input 
                         id="formation" type="text" name="formation" 
                         class="shadow border rounded w-full text-xl @error('formation') invalid @enderror" 
                         value="{{ old('formation') }}" 
                         autocomplete="formation" 
                         placeholder="formation" 
                         autofocus
                    >
                    @error('formation')
                         <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
         </div>

         <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- langue -->
               <div>
                    <label for="langue" class="block font-semibold text-gray-700 mb-2">Langue</label>
                    <input 
                         id="langue" type="text" name="langue" 
                         class="shadow border rounded w-full text-xl @error('langue') invalid @enderror" 
                         value="{{ old('langue') }}" 
                         autocomplete="langue" 
                         placeholder="Langue" 
                         autofocus
                    >
                    @error('langue')
                         <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>

               <!-- niveau_etude -->
               <div>
                    <label for="niveau_etude" class="block font-semibold text-gray-700 mb-2"> votre niveau d'étude</label>
                    <input 
                         id="niveau_etude" type="text" name="niveau_etude" 
                         class="shadow border rounded w-full text-xl @error('niveau_etude') invalid @enderror" 
                         value="{{ old('niveau_etude') }}" 
                         autocomplete="niveau_etude" 
                         placeholder="niveau d'étude" 
                         autofocus
                    >
                    @error('niveau_etude')
                         <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
         </div>


          <div class="mb-4 grid grid-cols-1 md:grid-cols-2 gap-6">
               <!-- competence -->
               <div> 
                    <label for="competence" class="block font-semibold text-gray-700 mb-2">competence</label>
                    <input 
                         id="competence" type="text" name="competence" 
                         class="shadow border rounded w-full text-xl @error('competence') invalid @enderror" 
                         value="{{ old('competence') }}" 
                         autocomplete="competence" 
                         placeholder="competence" 
                         autofocus
                    >
                    @error('competence')
                         <span class="text-red-400 text-sm">{{ $message }}</span>
                    @enderror
               </div>
          </div>

          <!-- Motivation  -->
          <div class="mb-4">
               <label for="motivation" class="block font-semibold text-gray-700 mb-2">Écrivez vos motivations</label>
               <textarea 
                    name="motivation" 
                    id="motivation" 
                    class="shadow border rounded w-full text-xl" cols="30" rows="10"
               >
                    {{ old('motivation') }}
               </textarea>
               @error('motivation')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>

          <!-- experience  -->
          <div class="mb-4">
               <label for="experience" class="block font-semibold text-gray-700 mb-2">Écrivez vos experiences</label>
               <textarea 
                    name="experience" 
                    id="experience" 
                    class="shadow border rounded w-full text-xl" cols="30" rows="10"
               >
                    {{ old('experience') }}
               </textarea>
               @error('experience')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>

         
          <!-- cv -->
          <div class="mb-4">
               <label for="cv" class="block font-semibold text-gray-700 mb-2">CV<span class="text-sm font-normal">(Image: jpeg, png, jpg)</span>:</label>
               <input type="file" name="cv" id="cv" class="form-control">
               @error('cv')
                    <span class="text-red-400 text-sm">{{ $message }}</span>
               @enderror
          </div>

          <div class="flex justify-between align-center">
               <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded">S'inscrire</button>
               <a href="{{route('candidat.login') }}" class="text-blue-400 text-lg hover:text-blue-600">Se connecter</a>
          </div>
     </form>
</div>
@push('scripts')
     <script src="https://cdn.ckeditor.com/ckeditor5/37.1.0/classic/ckeditor.js"></script>
     <script>
         ClassicEditor
             .create( document.querySelector( '#motivation' ) )
             .catch( error => {
                 console.error( error );
             } );

         ClassicEditor
             .create( document.querySelector( '#experience' ) )
             .catch( error => {
                 console.error( error );
             } );
     </script>
@endpush
@endsection