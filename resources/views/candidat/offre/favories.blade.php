@extends('layout-main.auth-main')

@section('content')

<div class="container-auth">

     <h1 class="mt-5 text-center underline mb-3 text-2xl">Vos Offres Favories</h1>
     @livewire('offre.offre-favori', ['secteurs' => $secteurs ])
     
</div>
@endsection