@extends('layout-main.auth-main')

@section('content')    
     <div class="container-auth">
          
          <h2 class="mt-5 font-bold mb-3 text-2xl capitalize">Offres Postulées</h2>
          @livewire('offre.filter-offers-postulated', ['secteurs' => $secteurs])

     </div>
@endsection