@extends('layout-main.guest-main')

@section('content')
<div class="container-guest container-guest--margin mx-auto rounded-lg border shadow-md p-5">
    
     <h1 class="text-3xl text-blue-500 mb-6 text-center">Mot de passe Oublié</h1>
     <form method="POST" action="{{ route('password.email') }}">
         @csrf

         @if ($errors->any())
               <div class="bg-red-400 p-3 text-center text-white text-xl mb-3">
                    <ul>
                         @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                         @endforeach
                    </ul>
               </div>
          @endif

          @if (session('message'))
               <div class="bg-green-400 p-3 text-center">
                    {{ session('message') }}
               </div>
          @endif
         
          <!-- Adresse email -->
         <div class="mb-4">
             <label for="email" class="block font-semibold text-gray-700 mb-2">Email</label>
             <input 
                 id="email" type="email" name="email" 
                 class="shadow border rounded w-full text-xl @error('email') invalid @enderror" 
                 value="{{ old('email') }}" 
                 autocomplete="email" 
                 placeholder="Votre email" autofocus
             >
             @error('email')
                 <span class="text-red-400 text-md">
                     <span>{{ $message }}</span>
                 </span>
             @enderror
         </div>
     
         <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded">Envoyer un email de reinitialisation</button>
     </form>
 </div>
@endsection