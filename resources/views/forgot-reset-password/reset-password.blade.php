@extends('layout-main.guest-main')

@section('content')
<div class="container-guest container-guest--margin mx-auto rounded-lg border shadow-md p-5">
    
     <h1 class="text-3xl text-blue-500 mb-6 text-center">Réinitialisation de mot de passe</h1>
     <form method="POST" action="{{ route('password.store') }}">
         @csrf

         @if (session('error'))
               <div class="bg-red-400 p-3 text-center text-white text-xl mb-3">
                    <ul>
                         <li>{{ session('error') }}</li>
                    </ul>
               </div>
          @endif

          @if (session('message'))
               <div class="bg-green-400 p-3 text-center">
                    {{ session('message') }}
               </div>
          @endif

          <input type="hidden" name="token" value="{{ $token }}">
         
          <!-- Email -->
         <div class="mb-4">
             <label for="email" class="block font-semibold text-gray-700 mb-2">Email</label>
             <input 
                 id="email" type="email" name="email" 
                 class="shadow border rounded w-full text-xl @error('email') invalid @enderror" 
                 value="{{ old('email') }}" 
                 autocomplete="email" 
                 placeholder="Votre email" autofocus
             >
             @error('email')
                 <span class="text-red-400 text-md">
                     <span>{{ $message }}</span>
                 </span>
             @enderror
         </div>
     
         <!-- Mot de passe -->
         <div class="mb-4">
             <label for="password" class="block font-semibold text-gray-700 mb-2">Mot de passe</label>
             <input 
                 id="password" type="password" name="password" 
                 class="shadow border rounded w-full text-xl @error('password') invalid @enderror" 
                 autocomplete="password" 
                 placeholder="Votre mot de passe" 
                 autofocus
             >
            @error('password')
                 <span class="text-red-400 text-sm">{{ $message }}</span>
             @enderror
         </div>

         <!-- Mot de passe Oubliée -->
         <div class="mb-4">
             <label for="password" class="block font-semibold text-gray-700 mb-2">Mot de passe</label>
             <input 
                 id="password" type="password" name="password_confirmation" 
                 class="shadow border rounded w-full text-xl @error('password_confirmation') invalid @enderror"  
                 autocomplete="password" 
                 placeholder="Votre mot de passe" 
                 autofocus
             >
            @error('password_confirmation')
                 <span class="text-red-400 text-sm">{{ $message }}</span>
             @enderror
         </div>
     
         <button class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded">Réinitialiser</button>

     </form>
 </div>
@endsection