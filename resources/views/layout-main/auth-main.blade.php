<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>@yield('title', 'Authentification')</title>
     <!-- Fonts -->
     <link rel="preconnect" href="https://fonts.bunny.net">
     <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-dcSag2W5o"/>
 
     
     @vite(['resources/css/app.css', 'resources/js/app.js'])
     <style>
          [x-cloak] { display: none !important; }
     </style>
     @livewireStyles
</head>
<body>
     
    @can('isAdmin')
        @include('partials.admin-navbar')
        @include('partials.cartes')
    @elsecan('isRecruteur')
        @include('partials.recruteur-navbar')
    @else
        @include('partials.candidat-navbar')
    @endcan


     @yield('content')

     @include('partials.admin-footer')
     
     @stack('scripts')

     @livewireScripts
</body>
</html>