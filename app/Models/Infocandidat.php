<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Infocandidat extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'profession',
        'formation',
        'langue',
        'niveau_etude',
        'competence',
        'motivation',
        'cv',
        'experience'
    ];
}
