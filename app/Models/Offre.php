<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Offre extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'titre_poste',
        'localisation_poste',
        'type_poste',
        'secteur_poste',
        'description',
        'email_candidature',
        'site_candidature',
        'salaire',
    ];

    // public static function boot() {
    //     parent::boot();
    //     static::creating(function ($model) {
    //         $model->user_id = auth()->user()->id;
    //     });
    // }

    // récuperer les candidats qui ont postulé à une offre
    // méthode que l'on peut utiliser dans la partie recruteur
    public function postulants(): BelongsToMany {
        return $this->belongsToMany(User::class, 'user_postuler_offre')->withPivot('validated', 'id');
    }

    // Récuperer les secteurs d'une offre
    public function secteurs(): BelongsToMany {
        return $this->belongsToMany(Secteur::class, 'offre_toucher_secteur');
    }

    public function isLiked() {
        if (Auth::check()) {
            return Auth::user()->offresfavories->contains('id', $this->id);
        }
    }

    public function user(): BelongsTo {
        return $this->belongsTo(User::class);
    }
}
