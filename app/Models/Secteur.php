<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Secteur extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'nom',
    ];

    // Récuperer les secteurs d'une offre
    public function offres(): BelongsToMany {
        return $this->belongsToMany(Offre::class, 'offre_toucher_secteur');
    }
}
