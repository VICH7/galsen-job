<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use ShiftOneLabs\LaravelCascadeDeletes\CascadesDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, CascadesDeletes;

    protected $cascadeDeletes = ['offresPubliees', 'infoRecruteur', 'infoCandidat'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nom',
        'prenom',
        'telephone',
        'image',
        'adresse',
        'ville',
        'role',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    // ne pas utiliser la méthode conversations() au lieu de cela utliser getConversationsAttribute pour récuperer les conversations
    public function conversations() {
        return Conversation::where(function ($q) {
            return $q->where('to', $this->id)
            ->orWhere('from', $this->id);
        })->orderBy('updated_at', 'desc');
    }
    
    // recuperer les conversations de l'utilisateur actuellement connecté(méthode basé sur la métode ci-dessus)
    public  function getConversationsAttribute()
    {
        return $this->conversations()->get();
    }

    
    // recuperer les offres favories de l útilisateur actuellement connecté
    public function offresfavories(): BelongsToMany {
        return $this->belongsToMany(Offre::class, 'user_aimer_offre');
    }

    // récuperer les offres postulées du candidat actuellement connecté
    public function candidatOffres(): BelongsToMany {
        return $this->belongsToMany(Offre::class, 'user_postuler_offre')->withPivot('validated');
    }

    // recuperer les offres publiés par l'utilisateur actuellement connecté
    public function offresPubliees(): HasMany {
        return $this->hasMany(Offre::class);
    }

    // recuperer les cvs ajoutés par l'admin actuellement connecté
    public function cvs(): HasMany {
        return $this->hasMany(Secteur::class);
    }

    // recuperer les secteurs ajoutés par l'admin actuellement connecté
    public function secteurs(): HasMany {
        return $this->hasMany(Secteur::class);
    }

    // récuperer les infos du recruteur actuellement connecté
    public function infoRecruteur(): HasOne {

        return $this->hasOne(Inforecruteur::class);
    }

    // récuperer les infos du candidat actuellement connecté
    public function infoCandidat(): HasOne {
        return $this->hasOne(Infocandidat::class);
    }
    
}
