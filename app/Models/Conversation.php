<?php

namespace App\Models;

use App\Models\Message;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Conversation extends Model
{
    use HasFactory;

    protected $fillable = [
        'from',
        'to',
    ];

    public function messages(): HasMany {
        return $this->hasMany(Message::class);
    }

    public function user(): BelongsTo {
        return $this->belongsTo(Message::class);
    }
    
}
