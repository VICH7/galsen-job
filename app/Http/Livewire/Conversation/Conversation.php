<?php

namespace App\Http\Livewire\Conversation;

use App\Models\Conversation as ModelsConversation;
use App\Models\User;
use App\Models\Message;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Livewire\Component;

class Conversation extends Component
{
    public $conversations, 
    $users = [], 
    $text_search = '', 
    $sendTo = null, 
    $text_message = '';

    public function updatedTextSearch() {
        $words = '%' . $this->text_search . '%';

        if (strlen($this->text_search) > 0) {
            $this->users = User::where('nom', '!=' ,auth()->user()->nom)
            ->where('prenom', '!=' ,auth()->user()->prenom)
            ->where(function ($query) use ($words) {
                $query->where('nom', 'like' , $words)
                ->orWhere('prenom', 'like' , $words);
            })->get();
        }
    }

    public function saveMessage() {

        $user_role = auth()->user()->role;

        if (strlen($this->text_message) > 0 && !is_null($this->sendTo)) {

            $existing_conversation = ModelsConversation::where('from', auth()->user()->id)
            ->where('to', $this->sendTo['id'])
            ->orWhere(function ($query) {
                return $query->where('from', $this->sendTo['id'])
                             ->where('to', $this->sendTo['id']);
            })->first();

            if ($existing_conversation) {
                Message::create([
                    'user_id' => auth()->user()->id,
                    'conversation_id' => $existing_conversation->id,
                    'contenu' => $this->text_message,
                ]);

                switch ($user_role) {
                    case 'admin':
                        redirect()->route('admin.auth.conversations.show', $existing_conversation->id);
                        return  $this->resetExcept('conversations');
                    default:
                        redirect()->route('recruteur.auth.conversations.show', $existing_conversation->id);
                    return  $this->resetExcept('conversations');
                }
            }

            $conversation = ModelsConversation::create([
                'from' => auth()->user()->id,
                'to' => $this->sendTo['id']
            ]);

            Message::create([
                'user_id' => auth()->user()->id,
                'conversation_id' => $conversation->id,
                'contenu' => $this->text_message,
            ]); 

            $this->resetExcept('conversations');

            switch ($user_role) {
                case 'admin':
                    return redirect()->route('admin.auth.conversations.show', $conversation->id);
                default:
                    return redirect()->route('recruteur.auth.conversations.show', $conversation->id);
            }
        }
    }

    public function render()
    {
        return view('livewire.conversation.conversation');
    }
}
