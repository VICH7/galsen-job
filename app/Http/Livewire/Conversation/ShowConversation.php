<?php

namespace App\Http\Livewire\Conversation;

use App\Models\Message;
use App\Models\User;
use Livewire\Component;

class ShowConversation extends Component
{
    public $conversation, $text_message, $to_user;

    protected $listeners=['send' => '$refresh'];

    public function mount() {
        $this->to_user = $this->conversation->to;
        $this->to_user = User::find($this->to_user);
        $this->to_user = $this->to_user->nom;
    }

    public function saveMessage() {
        if ($this->text_message != '') {
            
            $message = new Message();
            $message->user_id = auth()->user()->id;
            $message->conversation_id = $this->conversation->id;
            $message->contenu = $this->text_message;

            $message->save();

            $this->reset('text_message');
            $this->emit('send');
            $this->dispatchBrowserEvent('scrollDown');

        }
    }
    public function render()
    {
        return view('livewire.conversation.show-conversation');
    }
}
