<?php

namespace App\Http\Livewire\Secteur;

use App\Models\Secteur;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;

class Secteurs extends Component
{
    use WithPagination;

    public $search = '';

    public function updatedSearch() {
        $this->resetPage();
    }

    protected $queryString = [
        'search' => ['except' => '', 'as' => 's'],
    ];

    public function render()
    {
        $secteurs = Secteur::
        when($this->search, function (Builder $query) {
            $query->where('nom', 'like', '%'.$this->search.'%');
        })
        ->paginate(10);;
        return view('livewire.secteur.secteurs', compact('secteurs'));
    }
}
