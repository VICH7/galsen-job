<?php

namespace App\Http\Livewire\UsersManagement;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;

class Candidat extends Component
{
    use WithPagination;

    public $old = false, $search;

    protected $queryString = [
        'search' => ['except' => ''], 
    ];

    public function updatedSearch() {
        $this->resetPage();
    }

    public function old_or_recent() {
        if ($this->old == false) {
            return $this->old = true;
        }

        return $this->old = false; 
    }

    public function render()
    {
        $candidats = User::where('role', 'candidat')
        ->when($this->search, function (Builder $query) {
            $query->where('nom', 'like', '%'.$this->search.'%')
            ->orwhere('prenom', 'like', '%'.$this->search.'%')
            ->orwhere('email', 'like', '%'.$this->search.'%');
        })
        ->when($this->old, function (Builder $query) {
            $query->orderBy('id', 'asc');
        }, function (Builder $query) {
            $query->orderBy('id', 'desc');
        })
        ->paginate(10);
        return view('livewire.users-management.candidat', compact('candidats'));
    }
}
