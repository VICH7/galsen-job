<?php

namespace App\Http\Livewire\UsersManagement;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;

class Recruteur extends Component
{
    use WithPagination;

    public $old = false, $search;

    protected $queryString = [
        'search' => ['except' => ''], 
    ];

    public function updatedSearch() {
        $this->resetPage();
    }

    public function old_or_recent() {
        if ($this->old == false) {
            return $this->old = true;
        }

        return $this->old = false;
    }

    public function render()
    {
        $recruteurs = User::where('role', 'recruteur')
        ->when($this->search, function (Builder $query) {
            $query->where('nom', 'like', '%'.$this->search.'%')
            ->orwhere('prenom', 'like', '%'.$this->search.'%')
            ->orwhere('email', 'like', '%'.$this->search.'%');
        })
        ->when($this->old, function (Builder $query) {
            $query->orderBy('id', 'asc');
        }, function (Builder $query) {
            $query->orderBy('id', 'desc');
        })
        ->paginate(10);
        return view('livewire.users-management.recruteur', compact('recruteurs'));
    }
}
