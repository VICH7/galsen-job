<?php

namespace App\Http\Livewire\Offre;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class OffreFavori extends Component
{
    use WithPagination;

    public $old = false, $secteurs, $typePosteSelect = [], $secteursSelect = [];

    protected $queryString = [
        'typePosteSelect' => ['except' => '', 'as' => 'poste'], 
        'secteursSelect' => ['except' => '', 'as' => 'secteur'],
    ];

    public function updatedTypePosteSelect() {
        $this->resetPage();
    }
    public function updatedSecteursSelect() {
        $this->resetPage();
    }


    
    public function old_or_recent() {
        if ($this->old == false) {
            return $this->old = true;
        }

        return $this->old = false;
        
    }

    public function deleteLike($offre) {
        if (Auth::check()) {
            /** @var \App\Models\User $user **/
            $user = Auth::user();
            $response = $user->offresfavories()->detach($offre['id']);
        }
    }
    
    public function render()
    {
        $offres = Auth::user()->offresfavories()
        ->when($this->typePosteSelect, function (Builder $query) {
            $query->whereIn('type_poste', $this->typePosteSelect);
        })
        ->when($this->secteursSelect, function (Builder $query) {
            $query->whereHas('secteurs', function (Builder $query) {
                $query->whereIn('secteurs.id', $this->secteursSelect);
            });
        })
        ->when($this->old, function (Builder $query) {
            $query->orderBy('id', 'asc');
        }, function (Builder $query) {
            $query->orderBy('id', 'desc');
        })
        ->paginate(10);


        return view('livewire.offre.offre-favori', [
            'offres' => $offres
        ]);
    }
}
