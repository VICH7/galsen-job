<?php

namespace App\Http\Livewire\Offre;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class FilterOffersPostulated extends Component
{
    use WithPagination;

    public $secteurs, $secteursSelect = [], $typePosteSelect = [], $search;

    protected $queryString = [
        'search' => ['except' => '', 'as' => 's'],
        'typePosteSelect' => ['except' => '', 'as' => 'poste'], 
        'secteursSelect' => ['except' => '', 'as' => 'secteur'],
    ];

    public function updatedTypePosteSelect() {
        $this->resetPage();
    }

    public function updatedSecteursSelect() {
        $this->resetPage();
    }

    public function updatedSearch() {
        $this->resetPage();
    }

    public function render()
    {
        /** @var \App\Models\User $user **/
        $user = Auth::user();
        $offres = $user->candidatOffres()
        ->when($this->typePosteSelect, function (Builder $query) {
            $query->whereIn('type_poste', $this->typePosteSelect);
        })
        ->when($this->secteursSelect, function (Builder $query) {
            $query->whereHas('secteurs', function (Builder $query) {
                $query->whereIn('secteurs.id', $this->secteursSelect);
            });
        })
        ->when($this->search, function (Builder $query) {
            $query->where('titre_poste', 'like', '%'.$this->search.'%');
        })
        ->paginate(10);
        return view('livewire.offre.filter-offers-postulated', compact('offres'));
    }
}
