<?php

namespace App\Http\Livewire\Offre;

use App\Models\Offre;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Postulants extends Component
{
    use WithPagination, AuthorizesRequests;
    
    public $offre, $search = '';

    public function mount(Offre $offre) {
        $this->offre = $offre;
    }

    protected $listeners = ['refreshComponent' => '$refresh'];

    public function action(string $statut, int $potulantId) {

        $this->authorize('action', $this->offre);

        switch ($statut) {
            case 'accept':
                $this->offre->postulants()->updateExistingPivot($potulantId, [
                    'validated' => 'accept',
                ]);
                $this->emit('refreshComponent');
                break;
                
            case 'reject':
                $this->offre->postulants()->updateExistingPivot($potulantId, [
                    'validated' => 'reject',
                ]);
                $this->emit('refreshComponent');
                break;
            
            default:
                $this->offre->postulants()->updateExistingPivot($potulantId, [
                    'validated' => 'pending',
                ]);
                $this->emit('refreshComponent');
                break;
        }
    }

    

    public function updatedSearch() {
        $this->resetPage();
    }

    protected $queryString = [
        'search' => ['except' => '', 'as' => 's'],
    ];

    public function render()
    {
        $postulants = $this->offre->postulants()
        ->when($this->search, function (Builder $query) {
            $query->where('email', 'like', '%'.$this->search.'%')
                  ->orWhere('prenom', 'Like', '%'.$this->search.'%')
                  ->orWhere('nom', 'Like', '%'.$this->search.'%');
        })
        ->paginate(10);

        return view('livewire.offre.postulants', compact('postulants'));
    }
}
