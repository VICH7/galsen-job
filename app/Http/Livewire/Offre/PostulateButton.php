<?php

namespace App\Http\Livewire\Offre;

use Livewire\Component;

class PostulateButton extends Component
{
    protected $listeners = ['flash' => 'setFlashMessage'];

    public function setFlashMessage() {
        $this->dispatchBrowserEvent('flash-message');
    }
    
    public function render()
    {
        return <<<'blade'
        
            <div>
                <button wire:click="$emit('flash')" class="bg-blue-500 text-white hover:bg-blue-700 transition ease-in-out duration-500 rounded-md shadow-md text-lg p-1 text-white rounded uppercase my-4">
                    Postuler
                </button>
            </div>
        blade;
    }
}
