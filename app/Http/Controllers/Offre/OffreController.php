<?php

namespace App\Http\Controllers\Offre;

use App\Models\Offre;
use App\Models\Secteur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class OffreController extends Controller
{
    public function index() 
    {
        $secteurs = Secteur::all();

        if (request()->routeIs('admin.auth.offres')) {
            return view('admin.offre.offres', compact('secteurs'));
        }

        if (request()->routeIs('recruteur.auth.offres')) {
            return view('recruteur.offre.offres', compact('secteurs'));
        }

        return view('candidat.offre.offres', compact('secteurs'));

    }

    public function guestShowOffer(Offre $offre) 
    {
        // s'il est connecté
        if (Auth::check()) {
            return redirect()->back();
        }
        return view('guest-offer', compact('offre'));
    }

    public function guestOffers() 
    {
        // s'il est connecté
        if (Auth::check()) {
            return redirect()->back();
        }
        $secteurs = Secteur::all();
        return view('guest-offers', compact('secteurs'));
    }

    public function create() 
    {
        $secteurs = Secteur::all();

        if (request()->routeIs('admin.auth.offres.create')) {
            return view('admin.offre.create', compact('secteurs'));
        }

        if (request()->routeIs('recruteur.auth.offres.create')) {
            return view('recruteur.offre.create', compact('secteurs'));
        }
    }
    
    public function postulants(Offre $offre)
    {
        
        return view('admin.offre.postulants', compact('offre'));
    }

    public function postulate(Offre $offre) 
    {
        Gate::authorize('postulate');

        /** @var \App\Models\User $user **/
        $user = Auth::user();
        
        $query = $user->candidatOffres()
        ->wherePivot('user_id', auth()->user()->id)
        ->wherePivot('offre_id', $offre->id)->get();

        if ($query->count() == 0) {
            $user->candidatOffres()->attach($offre->id, ['recruteur_id' => $offre->user_id]);
            return redirect()->route('candidat.auth.offres.postulated');
        }
        
        return redirect()->route('candidat.auth.offres.postulated');
    }

    public function cancel_offer(Offre $offre) 
    {

        /** @var \App\Models\User $user **/
        $user = Auth::user();
        
        $query = $user->candidatOffres()
        ->wherePivot('user_id', auth()->user()->id)
        ->wherePivot('offre_id', $offre->id)->get();

        if ($query->count() > 0) {
            $user->candidatOffres()->detach($offre->id);
            return redirect()->route('candidat.auth.offres.postulated')->with('success', 'Annulée avec succès');
        }

        
    }

    public function offer_postuled() 
    {   
        $secteurs = Secteur::all();
        return view('candidat.offre.offer_postulated', compact('secteurs'));
    }

    public function update(Request $request, Offre $offre) 
    {
        $request->validate([
            'type_poste' => 'required',
            'titre_poste' => 'required',
            'localisation_poste'=> 'required',
            'description'=> 'required',
            'secteurs'=> 'required|min:1',
        ]);

        
        $offre->type_poste = $request->type_poste;
        $offre->titre_poste = $request->titre_poste;
        $offre->localisation_poste = $request->localisation_poste;
        $offre->description = $request->description;
        if ($request->email_candidature) {
            $offre->email_candidature = $request->email_candidature; 
        }
        if ($request->salaire) {
            $offre->salaire = $request->salaire;
        }

        if ($offre->isDirty()) {
            $offre->save();
            $offre->secteurs()->sync($request->secteurs);
            
            return redirect()->route('admin.auth.offres.show', ['offre' => $offre->id])->with('success','L\' offre a été mis à jour avec succès');
        }

        if (request()->routeIs('recruteur.auth.offres.update')) {
            return redirect()->route('recruteur.auth.offres.show', ['offre' => $offre->id]);
        }

    }

    public function store(Request $request) 
    {
        $request->validate([
            'type_poste' => 'required',
            'titre_poste' => 'required',
            'localisation_poste'=> 'required',
            'description'=> 'required',
            'secteurs'=> 'required|min:1',
        ]);

        $offre = new Offre();
        $offre->user_id = auth()->user()->id;
        $offre->type_poste = $request->type_poste;
        $offre->titre_poste = $request->titre_poste;
        $offre->localisation_poste = $request->localisation_poste;
        $offre->description = $request->description;
        if ($request->email_candidature) {
            $offre->email_candidature = $request->email_candidature; 
        }
        if ($request->salaire) {
            $offre->salaire = $request->salaire;
        }

        $offre->save();

        $offre->secteurs()->attach($request->secteurs);

        return redirect()->route('admin.auth.offres')->with('success', 'Offre publiée avec succès');
    }

    public function show(Offre $offre) 
    {
        Gate::authorize('candidat-recruteur-admin-model', $offre);   

        if (request()->routeIs('admin.auth.offres.show')) {
            return view('admin.offre.show', compact('offre'));
        }

        if (request()->routeIs('recruteur.auth.offres.show')) {
            
            return view('recruteur.offre.show', compact('offre'));
        }

        return view('candidat.offre.show', compact('offre'));

    }


    public function edit(Offre $offre) 
    {
        Gate::authorize('mofify', $offre);

        $allSecteurs = Secteur::all();
        $secteurs = $offre->secteurs;

        if (request()->routeIs('admin.auth.offres.edit')) {
            return view('admin.offre.edit', compact('offre', 'allSecteurs', 'secteurs'));
        }

        if (request()->routeIs('recruteur.auth.offres.edit')) {
            return view('recruteur.offre.edit', compact('offre', 'allSecteurs', 'secteurs'));
        }
    }
    

    public function mes_offres() 
    {
        $secteurs = Secteur::all();
        if (request()->routeIs('admin.auth.offres.mes-offres')) {
            return view('admin.offre.mes-offres', compact('secteurs'));
        }

        if (request()->routeIs('recruteur.auth.offres.mes-offres')) {
            return view('recruteur.offre.mes-offres', compact('secteurs'));
        }

    }

    public function offresFavories() 
    {
        $secteurs = Secteur::all();
        if (request()->routeIs('admin.auth.offres.favories')) {
            return view('admin.offre.favories', compact('secteurs'));
        }

        if (request()->routeIs('recruteur.auth.offres.favories')) {
            return view('recruteur.offre.favories', compact('secteurs'));
        }

        return view('candidat.offre.favories', compact('secteurs'));
    }

    public function destroy(Offre $offre) 
    {
        $offre->delete();
        
        if (request()->routeIs('admin.auth.offres.destroy')) {
            return redirect()->route('admin.auth.offres');
        }
        if (request()->routeIs('recruteur.auth.offres.destroy')) {
            return redirect()->route('recruteur.auth.offres');
        }
    }
}
