<?php

namespace App\Http\Controllers\Secteur;

use App\Http\Controllers\Controller;
use App\Models\Secteur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
// use App\Models\Secteur;


class SecteurController extends Controller
{
    public function index() {
        return view('admin.secteur.secteurs');
    }

    public function create() {
        return view('admin.secteur.create');
    }

    public function store(Request $request) {
        
        $request->validate([
            'nom' => 'required|min:3, max:255',
        ]);
    
        $secteur = new Secteur;
        $secteur->nom = $request->nom;
        $secteur->user_id= auth()->user()->id;
        $secteur->save();
    
        return redirect()->route('admin.auth.secteurs')->with('success','Le Secteur '. $secteur->nom . ' a été ajouté avec succès');

        // enregistrer des secteurs dans la base de données
    }

    public function edit(Secteur $secteur) {
        return view('admin.secteur.edit',compact('secteur'));
    }

    public function update(Request $request, Secteur $secteur) {
        $request->validate([
            'nom' => 'required',
        ]);
        
        $secteur->fill($request->post())->save();

        return redirect()->route('admin.auth.secteurs')->with('success','Le secteur a été mis à jour avec succès');
    
        
    }

    public function destroy(Secteur $secteur) {
        $secteur->delete();
        return redirect()->route('admin.auth.secteurs')->with('success','Le secteur a été supprimer avec succès !');
    }
}
