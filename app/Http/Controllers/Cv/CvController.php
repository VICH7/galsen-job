<?php

namespace App\Http\Controllers\Cv;

use App\Http\Controllers\Controller;
use App\Models\Cv;
use Illuminate\Http\Request;
use Storage;

class CvController extends Controller
{
    public function index()
    {
        $cvs = Cv::orderBy('id', 'desc')->get();
        if (request()->routeIs('admin.auth.cvs')) {
            return view('admin.cv.cvs', compact('cvs'));
        }

        if (request()->routeIs('recruteur.auth.cvs')) {
            return view('recruteur.cv.cvs', compact('cvs'));
        }

        return view('candidat.cv.cvs', compact('cvs'));
    }

    public function create()
    {
        if (request()->routeIs('admin.auth.cvs.create')) {
            return view('admin.cv.create');
        }   
        if (request()->routeIs('recruteur.auth.cvs.create')) {
            return view('recruteur.cv.create');
        }    
        if (request()->routeIs('candidat.auth.cvs.create')) {
            return view('candidat.cv.create');
        }
        
        // Ajoutez une instruction de retour par défaut si nécessaire
    }
    

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'exemple_cv' => 'required|file|mimes:doc,docx,pdf',
        ]);

        $image_file = $request->file('image')->hashName();
        $path_image = $request->file('image')->storeAs(
            'image_cvs',
            $image_file,
            'public'
        );

        $file_cv = $request->file('exemple_cv')->hashName();
        $path_cv = $request->file('exemple_cv')->storeAs(
            'file_cvs',
            $file_cv,
            'public'
        );

        Cv::create([
            'user_id' => auth()->user()->id,
            'image' => $path_image,
            'exemple_cv' => $path_cv,
        ]);

        if (request()->routeIs('admin.auth.cvs.store')) {
            return redirect()->route('admin.auth.cvs')->with('success','Le CV a été créé avec succès !');
        }   
        if (request()->routeIs('recruteur.auth.cvs.store')) {
            return redirect()->route('recruteur.auth.cvs')->with('success','Le CV a été créé avec succès !');
        }    
        if (request()->routeIs('candidat.auth.cvs.store')) {
            return redirect()->route('candidat.auth.cvs')->with('success','Le CV a été créé avec succès !');
        }
    }

    public function show(Cv $cv)
    {  
        if (request()->routeIs('admin.auth.cvs.show')) {
            return view('admin.cv.show', compact('cv'));
        }
        if (request()->routeIs('recruteur.auth.cvs.show')) {
            return view('recruteur.cv.show', compact('cv'));
        }
        return view('candidat.cv.show', compact('cv'));
    }

    public function destroy(CV $cv)
    {
        Storage::disk('public')->delete($cv->image);
        Storage::disk('public')->delete($cv->exemple_cv);
        $cv->delete();
        if (auth()->user()->role == 'admin') {
            return redirect()->route('admin.auth.cvs')->with('success','Le CV a été supprimé avec succès !');
        }
        
        return redirect()->route('recruteur.auth.cvs')->with('success','Le CV a été supprimé avec succès !');
    }
}
