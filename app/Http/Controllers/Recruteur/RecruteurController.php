<?php

namespace App\Http\Controllers\Recruteur;

use App\Models\User;
use App\Rules\UserType;
use Illuminate\Http\Request;
use App\Models\Inforecruteur;
use Illuminate\Validation\Rules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RecruteurController extends Controller
{
    public function parametres() {
        return view('recruteur.parametre.parametres');
    }

    public function profils() {
        return view('recruteur.profil.profils');
    }

    public function checkLogin(Request $request) {
        $request->validate([
            'email' => ['required', 'string', 'email', new UserType('recruteur')],
            'password' => ['required', 'string', 'min:8', 'max:30'],
        ], [
            'email.required' => 'le champ email est obligatoire.',
            'password.required' => 'le champ password est obligatoire.',
            'password.min' => 'Le champ du mot de passe doit contenir au moins 8 caractères.',
            'password.max' => 'Le champ du mot de passe doit contenir moins de 30 caractères.',
        ]);



        $creds = $request->only(['email', 'password']);

        if (Auth::attempt($creds)) {
            $request->session()->regenerate();
            return redirect()->route('recruteur.auth.offres.mes-offres');
        }
        else {
            return redirect()->route('recruteur.login')->with('fail', 'email ou mot de passe incorrect');
        }
    }

    
    public function storeRegister(Request $request) {
        $request->validate([
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'ville' => 'required|string',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            // 'telephone' => 'string',
            // 'adresse' => 'string',

            'nom_entreprise' => 'required|string',
            'logo_entreprise' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'secteur_entreprise' => 'string',
        ]);

        
        $recruteur = new User();
        $recruteur->nom = $request->nom;
        $recruteur->prenom = $request->prenom;
        $recruteur->email = $request->email;
        $recruteur->password = \Hash::make($request->password);
        $recruteur->role = 'recruteur';
        if ($request->telephone) {
            $recruteur->telephone = $request->telephone;
        }
        if ($request->adresse) {
            $recruteur->adresse = $request->adresse;
        }
        if ($request->ville) {
            $recruteur->ville = $request->ville;
        }
        // enregistrement du recruteur
        $recruteur->save();


        $infoRecruteur = new Inforecruteur();
        $infoRecruteur->user_id = $recruteur->id;
        $infoRecruteur->nom_entreprise = $request->nom_entreprise;
        if ($request->description_entreprise) {
            $infoRecruteur->description_entreprise = $request->description_entreprise;
        }

        if ($request->secteur_entreprise) {
            $infoRecruteur->secteur_entreprise = $request->secteur_entreprise;
        }

        if ($request->file('logo_entreprise')) {
            $logo_file = $request->file('logo_entreprise')->hashName();
            $path_logo = $request->file('logo_entreprise')->storeAs(
                'logos',
                $logo_file,
                'public'
            );

            $infoRecruteur->logo_entreprise = $path_logo;
        } 


        // enregistrement des infos du recruteur
        $infoRecruteur->save();

        $creds = $request->only('email', 'password');

        if (Auth::attempt($creds)) {
            $request->session()->regenerate();
            return redirect()->route('recruteur.auth.offres.mes-offres');
        }
        else {
            return redirect()->route('recruteur.login')->with('fail', 'email ou mot de passe incorrect');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
