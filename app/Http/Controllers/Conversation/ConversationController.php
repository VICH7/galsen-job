<?php

namespace App\Http\Controllers\Conversation;

use App\Models\Conversation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConversationController extends Controller
{
    public function index() {

        /** @var \App\Models\User $user **/
        $user = auth()->user();
        
        if (request()->routeIs('admin.auth.conversations')) {
            return view('admin.conversation.conversations', [
                'conversations' => $user->getConversationsAttribute()
            ]);
        }

        if (request()->routeIs('recruteur.auth.conversations')) {
            return view('recruteur.conversation.conversations', [
                'conversations' => $user->getConversationsAttribute()
            ]);
        }

        return view('candidat.conversation.conversations', [
            'conversations' => $user->getConversationsAttribute()
        ]);
    }

    public function show(Conversation $conversation) {

        if (request()->routeIs('admin.auth.conversations.show')) {
            return view('admin.conversation.show', compact('conversation'));
        }

        if (request()->routeIs('recruteur.auth.conversations.show')) {
            return view('recruteur.conversation.show', compact('conversation'));
        }

        return view('candidat.conversation.show', compact('conversation'));
    }
}
