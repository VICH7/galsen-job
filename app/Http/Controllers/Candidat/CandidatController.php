<?php

namespace App\Http\Controllers\Candidat;

use App\Models\User;
use App\Rules\UserType;
use App\Models\Infocandidat;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CandidatController extends Controller
{
    public function parametres() {
        return view('candidat.parametre.parametres');
    }

    public function profils() {
        return view('candidat.profil.profils');
    }

    public function checkLogin(Request $request) {
        $request->validate([
            'email' => ['required', 'string', 'email', new UserType('candidat')],
            'password' => ['required', 'string', 'min:8', 'max:30'],
        ], [
            'email.required' => 'le champ email est obligatoire.',
            'password.required' => 'le champ password est obligatoire.',
            'password.min' => 'Le champ du mot de passe doit contenir au moins 8 caractères.',
            'password.max' => 'Le champ du mot de passe doit contenir moins de 30 caractères.',
        ]);

        $creds = $request->only(['email', 'password']);

        if (Auth::attempt($creds)) {
            $request->session()->regenerate();
            return redirect()->route('candidat.auth.offres');
        }
        else {
            return redirect()->route('candidat.login')->with('fail', 'email ou mot de passe incorrect');
        }
    }


    public function storeRegister(Request $request) {
        $request->validate([
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'ville' => 'required|string',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            // 'telephone' => 'string',
            // 'adresse' => 'string',
            
            'cv' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        
        $candidat = new User();
        $candidat->nom = $request->nom;
        $candidat->prenom = $request->prenom;
        $candidat->email = $request->email;
        $candidat->password = \Hash::make($request->password);
        $candidat->role = 'candidat';
        if ($request->telephone) {
            $candidat->telephone = $request->telephone;
        }
        if ($request->adresse) {
            $candidat->adresse = $request->adresse;
        }
        if ($request->ville) {
            $candidat->ville = $request->ville;
        }
        // enregistrement du candidat
        $candidat->save();


        $infoCandidat = new Infocandidat();
        $infoCandidat->user_id = $candidat->id;
        if ($request->profession) {
            $infoCandidat->profession = $request->profession;
        }

        if ($request->formation) {
            $infoCandidat->formation = $request->formation;
        }

        if ($request->langue) {
            $infoCandidat->langue = $request->langue;
        }

        if ($request->niveau_etude) {
            $infoCandidat->niveau_etude = $request->niveau_etude;
        }

        if ($request->competence) {
            $infoCandidat->competence = $request->competence;
        }

        if ($request->motivation) {
            $infoCandidat->motivation = $request->motivation;
        }

        if ($request->experience) {
            $infoCandidat->experience = $request->experience;
        }

        if ($request->file('cv')) {
            $extension = $request->file('cv')->getClientOriginalExtension();
            $file_cv = $request->file('cv')->hashName();

            if ($extension == 'png'  || $extension == 'jpg'  || $extension == 'jpeg') {
                $path_cv = $request->file('cv')->storeAs(
                    'image_cvs',
                    $file_cv,
                    'public'
                );
            } 
            else {
                $path_cv = $request->file('cv')->storeAs(
                    'file_cvs',
                    $file_cv,
                    'public'
                );
            }

            $infoCandidat->cv = $path_cv;
        }
        else {
            $infoCandidat->cv = null;
        }

        // enregistrement des infos du candidat
        $infoCandidat->save();

        $creds = $request->only('email', 'password');

        if (Auth::attempt($creds)) {
            $request->session()->regenerate();
            return redirect()->route('candidat.auth.offres');
        }
        else {
            return redirect()->route('candidat.login')->with('fail', 'email ou mot de passe incorrect');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
