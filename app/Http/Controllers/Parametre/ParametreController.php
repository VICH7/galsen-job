<?php

namespace App\Http\Controllers\Parametre;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class ParametreController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit() {
        if (request()->routeIs('admin.auth.parametres')) {
            return view('admin.parametre.edit');
        }

        if (request()->routeIs('recruteur.auth.parametres')) {
            return view('recruteur.parametre.edit');
        }

        return view('candidat.parametre.edit');
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'telephone' => 'required',
            'adresse' => 'required',
            'ville' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $user = $request->user();
        $user->nom = $request->input('nom');
        $user->prenom = $request->input('prenom');
        $user->telephone = $request->input('telephone');
        $user->adresse = $request->input('adresse');
        $user->email = $request->input('email');
        $user->ville = $request->input('ville');

        if ($request->file('image')) {
            $image_file = $request->file('image')->hashName();
            $path_image = $request->file('image')->storeAs(
                'images_profil',
                $image_file,
                'public'
            );

            $user->image = $path_image;
        }
    
        if ($request->user()->isDirty('email')) {
            $user->email_verified_at = null;
        }
    
        $user->save();

        switch ($user->role) {
            case 'admin':
                return redirect()->route('admin.auth.profils')->with('success', 'Mise à jour du profil réussie !');
            case 'recruteur':
                return redirect()->route('recruteur.auth.profils')->with('success', 'Mise à jour du profil réussie !');
            
            default:
            return redirect()->route('candidat.auth.profils')->with('success', 'Mise à jour du profil réussie !');
        }

    
    }
    
    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }
}
