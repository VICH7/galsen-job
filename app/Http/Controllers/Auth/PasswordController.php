<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rules\Password;

class PasswordController extends Controller
{
    /**
     * Update the user's password.
     */
    public function update(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'current_password' => ['required'],
            'password' => ['required', 'confirmed'],
        ]);

        if(!Hash::check($request->current_password, auth()->user()->password)){
            return back()->with("error", "Le mot de passe actuelle ne correspond pas");
        }

        $request->user()->update([
            'password' => Hash::make($validated['password']),
        ]);

        switch (auth()->user()->role) {
            case 'admin':
                return redirect()->route('admin.auth.parametres')->with('success', 'Mise à jour du mot de passe réussi !');
            case 'recruteur':
                return redirect()->route('recruteur.auth.parametres')->with('success', 'Mise à jour du mot de passe réussi !');
            default:
                return redirect()->route('candidat.auth.parametres')->with('success', 'Mise à jour du mot de passe réussi !');
        }

    }
}
