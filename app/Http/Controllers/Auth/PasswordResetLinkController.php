<?php

namespace App\Http\Controllers\Auth;

use Mail;
use Carbon\Carbon;
use Illuminate\View\View;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\ForgotPasswordMail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Password;

class PasswordResetLinkController extends Controller
{
    /**
     * Display the password reset link request view.
     */
    public function create(): View
    {
        // return view('auth.forgot-password');
        return view('forgot-reset-password.forgot-password');
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'email' => ['required', 'email'],
        ]);

        $token = Str::random(64);

        $forgotPasswordExists = DB::table('password_reset_tokens')
        ->where('email', $request->email)
        ->first();

        if ($forgotPasswordExists) {
            return back()->withErrors(
                'Vous avez déjà demander un changement de mot de passe pour cette email. Veuillez regarder dans votre addresse email'
            );
        }
  
        DB::table('password_reset_tokens')->insert([
            'email' => $request->email, 
            'token' => $token, 
            'created_at' => Carbon::now()
        ]);

        Mail::to($request->email)->send(new ForgotPasswordMail($token));

        return back()->with(
            'message', 'Veuillez regarder dans votre boite mail un lien de réinitialisation vous a été envoyé'
        );

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        // $status = Password::sendResetLink(
        //     $request->only('email')
        // );

        // return $status == Password::RESET_LINK_SENT
        //             ? back()->with('status', __($status))
        //             : back()->withInput($request->only('email'))
        //                     ->withErrors(['email' => __($status)]);
    }
}
