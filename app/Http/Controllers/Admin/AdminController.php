<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\UserType;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{

    public function candidats() {

        return view('admin.candidat.candidats');
    }

    public function recruteurs() {
        return view('admin.recruteur.recruteurs');
    }

    public function checkLogin(Request $request) {
        $request->validate([
            'email' => ['required', 'string', 'email', new UserType('admin')],
            'password' => ['required', 'string', 'min:8', 'max:30'],
        ], [
            'email.required' => 'le champ email est obligatoire.',
            'password.required' => 'le champ password est obligatoire.',
            'password.min' => 'Le champ du mot de passe doit contenir au moins 8 caractères.',
            'password.max' => 'Le champ du mot de passe doit contenir moins de 30 caractères.',
        ]);

        // $invalid_user_role = User::where('email', $request->email)
        // ->where('role', 'admin')->first();

        $creds = $request->only(['email', 'password']);

        if (Auth::attempt($creds)) {
            $request->session()->regenerate();
            return redirect()->route('admin.auth.offres');
        }
        else {
            return redirect()->route('admin.login')->with('fail', 'email ou mot de passe incorrect');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

    public function destroyUser(User $user) {
        $user->delete();
        
        if ($user->trashed()) {
            return back()->with('success', 'Utilisateur supprimé avec succès');
        }
    }
}
