<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Storage;

class ProfileController extends Controller
{
    public function index() {
        if (request()->routeIs('admin.auth.profils')) {
            return view('admin.profil.profils');
        }
        if (request()->routeIs('recruteur.auth.profils')) {
            return view('recruteur.profil.profils');
        }

        return view('candidat.profil.profils');
    }

    public function edit() {
        if (request()->routeIs('admin.auth.profils.edit')) {
            return view('admin.profil.edit');
        }
        if (request()->routeIs('recruteur.auth.profils.edit')) {
            return view('recruteur.profil.edit');
        }

        return view('candidat.profil.edit');
    }


    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'telephone' => 'required',
            'adresse' => 'required',
            'ville' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
        $user = $request->user();
        $user->nom = $request->input('nom');
        $user->prenom = $request->input('prenom');
        $user->telephone = $request->input('telephone');
        $user->adresse = $request->input('adresse');
        $user->ville = $request->input('ville');

        if ($user->image != '' || $user->image != null) {
            Storage::disk('public')->delete($user->image);  
        } 

        if ($request->file('image')) {
            $image_file = $request->file('image')->hashName();
            $path_image = $request->file('image')->storeAs(
                'images_profil',
                $image_file,
                'public'
            );

            $user->image = $path_image;
        } 
        else {
            $user->image = null;
        }
    
        if ($request->user()->isDirty('email')) {
            $user->email_verified_at = null;
        }
    
        $user->save();

        switch (auth()->user()->role) {
            case 'admin':
                return redirect()->route('admin.auth.profils')->with('success', 'Mise à jour du profil réussie !');
            
                case 'recruteur':
                return redirect()->route('recruteur.auth.profils')->with('success', 'Mise à jour du profil réussie !');
        
            default:
            return redirect()->route('candidat.auth.profils')->with('success', 'Mise à jour du profil réussie !');
        }
    
    }
    

    /**
     * Update the user's additional information.
     */
    public function updateAdditional(Request $request): RedirectResponse
    {
        if (request()->routeIs('recruteur.auth.profils.updateAdditional')) {
            self::updateInfoRecruteur($request);
            return redirect()->route('recruteur.auth.profils')->with('success', 'Mise à jour du profil réussie !');
        } 

        if (request()->routeIs('candidat.auth.profils.updateAdditional')) {
            
            self::updateInfoCandidat($request);
            return redirect()->route('candidat.auth.profils')->with('success', 'Mise à jour du profil réussie !');
        } 
           
    }

    private function updateInfoRecruteur(Request $request) {
        $request->validate([
            'nom_entreprise' => 'required|string',
            'logo_entreprise' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $infoRecruteur = auth()->user()->infoRecruteur;
        $infoRecruteur->nom_entreprise = $request->nom_entreprise;
        if ($request->description_entreprise) {
            $infoRecruteur->description_entreprise = $request->description_entreprise;
        }

        if ($infoRecruteur->logo_entreprise != '' || $infoRecruteur->logo_entreprise != null) {
            Storage::disk('public')->delete($infoRecruteur->logo_entreprise);  
        } 

        if ($request->file('logo_entreprise')) {
            $image_file = $request->file('logo_entreprise')->hashName();
            $path_image = $request->file('logo_entreprise')->storeAs(
                'logos',
                $image_file,
                'public'
            );

            $infoRecruteur->logo_entreprise = $path_image;
        }
        else {
            $infoRecruteur->logo_entreprise = null;
        }

        if ($infoRecruteur->isDirty()) {
            $infoRecruteur->save();
        }
    }

    public function updateInfoCandidat(Request $request) {
        $request->validate([
            'cv' => 'mimes:jpeg,png,jpg,doc,docx,pdf|max:2048',
        ]);

        $infoCandidat = auth()->user()->infoCandidat;
        if ($request->profession) {
            $infoCandidat->profession = $request->profession;
        }
        if ($request->formation) {
            $infoCandidat->formation = $request->formation;
        }
        if ($request->langue) {
            $infoCandidat->langue = $request->langue;
        }
        if ($request->niveau_etude) {
            $infoCandidat->niveau_etude = $request->niveau_etude;
        }
        if ($request->competence) {
            $infoCandidat->competence = $request->competence;
        }
        if ($request->motivation) {
            $infoCandidat->motivation = $request->motivation;
        }
        if ($request->experience) {
            $infoCandidat->experience = $request->experience;
        }

        
        if ($infoCandidat->cv != '' || $infoCandidat->cv != null) {
            Storage::disk('public')->delete($infoCandidat->cv);  
        }

        if ($request->file('cv')) {
            $extension = $request->file('cv')->getClientOriginalExtension();
            $file_cv = $request->file('cv')->hashName();

            if ($extension == 'png'  || $extension == 'jpg'  || $extension == 'jpeg') {
                $path_cv = $request->file('cv')->storeAs(
                    'image_cvs',
                    $file_cv,
                    'public'
                );
            } 
            else {
                $path_cv = $request->file('cv')->storeAs(
                    'file_cvs',
                    $file_cv,
                    'public'
                );
            }

            $infoCandidat->cv = $path_cv;
        }
        else {
            $infoCandidat->cv = null;
        }

        if ($infoCandidat->isDirty()) {
            $infoCandidat->save();
        }
    }

    public function userProfil(User $user) {
        if ($user->role == 'recruteur') {
            return view('user-profil.recruteur-profil', compact('user'));
        }
        return view('user-profil.candidat-profil', compact('user'));
    }
    
}
