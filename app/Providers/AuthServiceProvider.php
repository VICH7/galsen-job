<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Offre;
use App\Models\User;
use App\Policies\OfferPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\Access\Response;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Offre::class => OfferPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('isAdmin', function($user){
            return $user->role == 'admin';
        });

        Gate::define('isCandidat', function($user){
            return $user->role == 'candidat';
        });

        Gate::define('isRecruteur', function($user){
            return $user->role == 'recruteur';
        });

        // permettre si c'est l'admin ou le recruteur approprié
        Gate::define('recruteur-admin-model', function($user, $model) {
            return $model->user_id == $user->id || $user->role == 'admin';
        });

        Gate::define('candidat-recruteur-admin-model', function($user, $model) {
            return $model->user_id == $user->id || $user->role == 'admin' || $user->role == 'candidat';
        });

        // permettre si c'est  le recruteur ou admin approprié
        Gate::define('mofify', function(User $user, $model) {
            return $user->id == $model->user_id  ? Response::allow() : Response::deny('Vous n\'êtes pas autorisé à modifier');
        });

        // permettre seulement au candidat de postuler
        Gate::define('postulate', function($user) {
            return $user->role == 'candidat' ? Response::allow() : Response::deny('Vous n\'êtes pas autorisé à postuler');
        });

        // Gate::before(function (User $user) {
        //     if ($user->role == 'admin') {
        //         return true;
        //     }
        // });
        
    }
}
