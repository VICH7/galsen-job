<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Offre;
use App\Models\Secteur;
use Illuminate\View\View;
use Illuminate\Support\Facades;
use Illuminate\Support\ServiceProvider;

class StatisticsCardServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        Facades\View::composer('partials.cartes', function (View $view) {
            $view
            ->with('offers_number', Offre::count())
            ->with('candidats_number', User::where('role', 'candidat')->get()->count())
            ->with('recruteurs_number', User::where('role', 'recruteur')->get()->count())
            ->with('secteurs_number', Secteur::count());
        });
    }
}
