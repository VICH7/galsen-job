<?php

namespace App\View\Components\ConversationsCard;

use App\Models\Conversation;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Conversations extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public Conversation $conversation
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.conversations-card.conversations');
    }
}
