<?php

namespace App\View\Components\Cvs;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class showAllCvs extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public $cvs
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.cvs.show-all-cvs');
    }
}
