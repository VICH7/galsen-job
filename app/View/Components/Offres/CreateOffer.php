<?php

namespace App\View\Components\Offres;

use App\Models\Secteur;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CreateOffer extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public $secteurs
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.offres.create-offer');
    }
}
