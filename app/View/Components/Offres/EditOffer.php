<?php

namespace App\View\Components\Offres;

use App\Models\Offre;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class EditOffer extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public Offre $offre,
        public $allSecteurs,
        public $secteurs,
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.offres.edit-offer');
    }
}
