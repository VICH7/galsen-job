<?php

namespace App\Rules;

use Closure;
use App\Models\User;
use Illuminate\Contracts\Validation\ValidationRule;

class UserType implements ValidationRule
{
    public function __construct(
        public string $type
    )
    {
        
    }
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $invalid_user_role = User::where('email', $value)
        ->where('role', $this->type)->first();

        if (!$invalid_user_role) {
            $fail('cette email :attribute ne figure pas dans notre section de base de donnée.');
        }
    }
}
