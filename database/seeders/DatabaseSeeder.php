<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Infocandidat;
use App\Models\Inforecruteur;
use App\Models\Offre;
use App\Models\Secteur;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Sequence;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);



        // creation de l'admin
        $admin =User::factory()
        ->state([
            'nom' => 'aly',
            'prenom' => 'lô',
            'telephone' => '771234567',
            'adresse' => 'dakar plateau',
            'ville' => 'dakar',
            'role' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('12345678'),
        ])->create();

        // creation de secteurs
        $secteurs = Secteur::factory()
        ->count(4)
        ->state(
            ['user_id' => $admin->id]
        )
        ->sequence(
            ['nom' => 'Administration publique'], // 1
            ['nom' => 'Agriculture, pêche, aquaculture'], // 2
            ['nom' => 'Aéronautique, navale'], // 3
            ['nom' => 'Informatique, réseaux'], // 4
        )->create();

        // creation d'offres pour l'admin
        $offres =  Offre::factory()
        ->count(5)
        ->state(
            ['user_id' => $admin->id]
        )
        ->sequence(
            ['type_poste' => 'cdd'],
            ['type_poste' => 'cdi'],
            ['type_poste' => 'stage'],
        )->create();

        // attacher des offres à des secteurs
        $offres->each(function($offre) use($secteurs) {
            $offre->secteurs()->attach($secteurs->random());
        });


        // creation de 15 recruteurs ainsi que certains de leurs infos
        $recruteurs = User::factory()
            ->count(15)
            ->state(['role' => 'recruteur'])
            ->has(
                Inforecruteur::factory()
                ->state(function (array $attributes, User $user) {
                    return ['user_id' => $user->id];
                }),
                'infoRecruteur'
        )->create();

        // creation d'offres pour chaque recruteurs et leurs associer des secteurs
        $recruteurs->each(function($recruteur) use($secteurs)  {
            Offre::factory()
            ->count(5)
            ->state(
                ['user_id' => $recruteur->id]
            )
            ->sequence(
                ['type_poste' => 'cdd'],
                ['type_poste' => 'cdi'],
                ['type_poste' => 'stage'],
            )->create()
            ->each(function($offre) use($secteurs) {
                $offre->secteurs()->attach($secteurs->random());
            });
        });


        // creation de 15 candidats ainsi que certains de leurs infos
        $candidats = User::factory()
            ->count(15)
            ->state(['role' => 'candidat'])
            ->has(
                Infocandidat::factory()
                ->state(function (array $attributes, User $user) {
                    return ['user_id' => $user->id];
                }),
                'infoCandidat'
            )
        ->create();
        
    }
}
