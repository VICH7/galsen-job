<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\InfoRecruteur>
 */
class InfoRecruteurFactory extends Factory
{
    private function random_entreprise_name() {
        $entreprises = [
            'Ottman Building cars',
            'Senegindia',
            'Google',
            'Volkeno',
            'Yassir',
            'Jumia',
        ];

        $random_keys=array_rand($entreprises, 1);
        return $entreprises[$random_keys];
    }
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nom_entreprise' => $this->random_entreprise_name(),
            'description_entreprise' => fake()->text(500),
        ];
    }
}
