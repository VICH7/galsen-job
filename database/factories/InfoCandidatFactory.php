<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\InfoCandidat>
 */
class InfoCandidatFactory extends Factory
{

    private function random_field(string $field) {

        $professions = [
            'developpeur web',
            'marketeur',
            'designer',
            'domestique',
            'jardinier'
        ];

        $formations = [
            'Université Virtuelle du Sénégal(UVS)',
            'Université Chekh Anta Diop(UCAD)',
            'Université Catholique de l\'Afrique de l\'Ouest(UCAO)',
            'Université Gaston Berger(UGB)',
        ];

        $niveaux_etude = [
            'bac+1',
            'bac+2',
            'master 2',
            'bac+5',
        ];

        if ($field === 'formation') {
            $random_keys = array_rand($formations, 1);
            return $formations[$random_keys];
        }

        if ($field === 'profession') {
            $random_keys = array_rand($professions, 1);
            return $professions[$random_keys];
        }

        if ($field === 'niveau_etude') {
            $random_keys = array_rand($niveaux_etude, 1);
            return $niveaux_etude[$random_keys];
        }
    }
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'profession' => $this->random_field('profession'),
            'formation' => $this->random_field('formation'),
            'langue'=>'Wolof, Français, Anglais',
            'niveau_etude'=>$this->random_field('niveau_etude'),
        ];
    }
}
