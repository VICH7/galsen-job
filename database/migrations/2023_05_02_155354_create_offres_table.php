<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('titre_poste');
            $table->string('localisation_poste');
            $table->enum('type_poste', ['cdd', 'cdi', 'stage']);
            // $table->string('secteur_poste');
            $table->text('description');
            $table->string('email_candidature')->nullable();
            $table->string('site_candidature')->nullable();
            $table->string('salaire')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offres');
    }
};
