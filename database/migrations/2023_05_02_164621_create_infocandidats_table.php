<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('infocandidats', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('profession')->nullable();
            $table->string('formation')->nullable();
            $table->string('langue')->nullable();
            $table->string('niveau_etude')->nullable();
            $table->string('competence')->nullable();
            $table->text('motivation')->nullable();
            $table->text('experience')->nullable();
            $table->string('cv')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('infocandidats');
    }
};
