## Table of Contents

1. [General info](#general-info)
2. [Technologies](#technologies)
3. [Project Team](#project-team)
4. [Installation](#installation)

## General info

The application name is __galsen-job__. She aims to create a job search site for anyone wishing to find work. So he can search for the job that suits him and that he likes through the offers and discover a lot of other things.
This application contains three users types: Admin, Recruiter and Candidate with differents UI.
Of course, do not forget authentication.

## Technologies

* [Laravel](https://laravel.com/): Version 10.8 
* [livewire](https://laravel-livewire.com/): Version 2.12
* [Alpine js](https://alpinejs.dev/): Version 3.4.2
* [tailwindcss](https://tailwindcss.com/): Version 3.1.0
* We use [shiftonelabs/laravel-cascade-deletes](https://packagist.org/packages/shiftonelabs/laravel-cascade-deletes): Version 2.0

## Project Team

- [Henry Ibrahima Vincent Coly](https://gitlab.com/VICH7)
- [Papa Alioune Gueye](https://gitlab.com/papaaliounegueye2000)
- [Alpha Oumar Barry](https://gitlab.com/alphaoumar9231)
- [Falla Fall](https://gitlab.com/fallafall)
- [Lamine Sagne](https://gitlab.com/papasagne301)

## Installation

- Clone project with: `git clone https://gitlab.com/VICH7/galsen-job.git`
- Run on your cmd or terminal : `composer install`
- Create an __env__ file and copy everything in the __.env.example__ file into the __.env__ file.
Open your __.env__ file and change the database name (__DB_DATABASE__) to whatever you have, username (__DB_USERNAME__) and password (__DB_PASSWORD__) field correspond to your configuration.
- Run: `php artisan key:generate`
- Run: `npm install`
- Run: `php artisan migrate`
- Run: `php artisan db:seed`
- Run: `npm run dev`
- Run: `php artisan serve`