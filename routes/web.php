<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Cv\CvController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Offre\OffreController;
use App\Http\Controllers\Profile\ProfileController;
use App\Http\Controllers\Secteur\SecteurController;
use App\Http\Controllers\Candidat\CandidatController;
use App\Http\Controllers\Parametre\ParametreController;
use App\Http\Controllers\Recruteur\RecruteurController;
use App\Http\Controllers\Conversation\ConversationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::prefix('admin')->name('admin.')->group(function () {

    Route::middleware(['guest', 'PreventBackHistory'])->group(function () {
        Route::view('/login', 'admin.auth.login')->name('login');
        Route::post('/check', [AdminController::class, 'checkLogin'])->name('check.login');
    });

    Route::middleware(['auth', 'auth-role:admin', 'PreventBackHistory'])->group(function () {

        Route::controller(OffreController::class)->group(function () {
            Route::get('/offres', 'index')->name('auth.offres');
            Route::get('/offres/favories', 'offresFavories')->name('auth.offres.favories');
            Route::get('/offres/mes-offres', 'mes_offres')->name('auth.offres.mes-offres');
            Route::get('/offres/create', 'create')->name('auth.offres.create');
            Route::post('/offres/store', 'store')->name('auth.offres.store');
            Route::get('/offres/{offre}/postulants', 'postulants')->name('auth.offres.postulants');
            Route::get('/offres/{offre}', 'show')->name('auth.offres.show');
            Route::put('/offres/{offre}', 'update')->name('auth.offres.update');
            Route::delete('/offres/{offre}', 'destroy')->name('auth.offres.destroy');
            Route::get('/offres/{offre}/edit', 'edit')->name('auth.offres.edit');
        });

        Route::controller(ConversationController::class)->group(function () {
            Route::get('/conversations', 'index')->name('auth.conversations');
            Route::get('/conversations/{conversation}', 'show')->name('auth.conversations.show');
        });

        Route::controller(SecteurController::class)->group(function () {
            Route::get('/secteurs', 'index')->name('auth.secteurs');
            Route::get('/secteurs/create', 'create')->name('auth.secteurs.create');
            Route::post('/secteurs/store', 'store')->name('auth.secteurs.store');
            Route::get('/secteurs/edit/{secteur}', 'edit')->name('auth.secteurs.edit');
            Route::put('/secteurs/update/{secteur}', 'update')->name('auth.secteurs.update');
            Route::delete('/secteurs/delete/{secteur}', 'destroy')->name('auth.secteurs.destroy');
        });

        Route::controller(CvController::class)->group(function () {
            Route::get('/cvs', 'index')->name('auth.cvs');
            Route::get('/cvs/create', 'create')->name('auth.cvs.create');
            Route::post('/cvs/store', 'store')->name('auth.cvs.store');
            Route::get('/cvs/show/{cv}', 'show')->name('auth.cvs.show');
            Route::delete('/cvs/delete{cv}', 'destroy')->name('auth.cvs.destroy');
        });

        Route::controller(ParametreController::class)->group(function() {
            Route::get('/moi/parametres', 'edit')->name('auth.parametres');
            Route::patch('/parametre', 'update')->name('parametre.update');
            Route::delete('/parametre', 'destroy')->name('parametre.destroy');
        });


        Route::controller(AdminController::class)->group(function() {
            Route::get('/candidats', 'candidats')->name('auth.candidats');
            Route::get('/recruteurs', 'recruteurs')->name('auth.recruteurs');
            Route::delete('/candidats/{user}', 'destroyUser')->name('auth.candidats.destroy');
            Route::delete('/recruteurs/{user}', 'destroyUser')->name('auth.recruteurs.destroy');
            // Route::get('/moi/profils', 'profils')->name('auth.profils');
            Route::post('/logout', 'logout')->name('auth.logout');
        });

        Route::controller(ProfileController::class)->group(function() {
            Route::get('/moi/profils', 'index')->name('auth.profils');
            Route::get('moi/profils/edit', 'edit')->name('auth.profils.edit');
            Route::patch('moi/profils/update', 'update')->name('auth.profils.update');
            Route::get('profils/user/{user}', 'userProfil')->name('auth.profils.userProfil');
        });

    });
});

Route::prefix('recruteur')->name('recruteur.')->group(function () {

    Route::middleware(['guest', 'PreventBackHistory'])->group(function () {
        Route::view('/login', 'recruteur.auth.login')->name('login');
        Route::view('/register', 'recruteur.auth.register')->name('register');
        Route::post('/register', [RecruteurController::class, 'storeRegister'])->name('store.register');
        Route::post('/check', [RecruteurController::class, 'checkLogin'])->name('check.login');
    });

    Route::middleware(['auth', 'auth-role:recruteur', 'PreventBackHistory'])->group(function () {

        Route::controller(OffreController::class)->group(function () {
            // Route::get('/offres', 'index')->name('auth.offres');
            // Route::get('/offres/favories', 'offresFavories')->name('auth.offres.favories');
            Route::get('/offres/mes-offres', 'mes_offres')->name('auth.offres.mes-offres');
            Route::get('/offres/create', 'create')->name('auth.offres.create');
            Route::post('/offres/store', 'store')->name('auth.offres.store');
            Route::get('/offres/{offre}/postulants', 'postulants')->name('auth.offres.postulants');
            Route::get('/offres/{offre}', 'show')->name('auth.offres.show');
            Route::put('/offres/{offre}', 'update')->name('auth.offres.update');
            Route::get('/offres/{offre}/edit', 'edit')->name('auth.offres.edit');
            Route::delete('/offres/{offre}', 'destroy')->name('auth.offres.destroy');
        });


        Route::controller(ConversationController::class)->group(function () {
            Route::get('/conversations', 'index')->name('auth.conversations');
            Route::get('/conversations/{conversation}', 'show')->name('auth.conversations.show');
        });

        Route::controller(CvController::class)->group(function () {
            Route::get('/cvs', 'index')->name('auth.cvs');
            Route::get('/cvs/create', 'create')->name('auth.cvs.create');
            Route::post('/cvs/store', 'store')->name('auth.cvs.store');
            Route::get('/cvs/show/{cv}', 'show')->name('auth.cvs.show');
            Route::delete('/cvs/delete{cv}', 'destroy')->name('auth.cvs.destroy');
        });


        Route::controller(ProfileController::class)->group(function() {
            Route::get('/moi/profils', 'index')->name('auth.profils');
            Route::get('moi/profils/edit', 'edit')->name('auth.profils.edit');
            Route::patch('moi/profils/update', 'update')->name('auth.profils.update');
            Route::patch('moi/profils/update-additional', 'updateAdditional')->name('auth.profils.updateAdditional');
            Route::get('profils/user/{user}', 'userProfil')->name('auth.profils.userProfil');
        });

        Route::controller(ParametreController::class)->group(function() {
            Route::get('/moi/parametres', 'edit')->name('auth.parametres');
            Route::patch('/parametre', 'update')->name('parametre.update');
            Route::delete('/parametre', 'destroy')->name('parametre.destroy');
        });

        Route::controller(ProfileController::class)->group(function() {
            Route::get('/moi/profils', 'index')->name('auth.profils');
            Route::get('moi/profils/edit', 'edit')->name('auth.profils.edit');
            Route::patch('moi/profils/update', 'update')->name('auth.profils.update');
            Route::patch('moi/profils/update-additional', 'updateAdditional')->name('auth.profils.updateAdditional');
            Route::get('profils/user/{user}', 'userProfil')->name('auth.profils.userProfil');
        });

        Route::controller(ParametreController::class)->group(function() {
            Route::get('/moi/parametres', 'edit')->name('auth.parametres');
            Route::patch('/parametre', 'update')->name('parametre.update');
            Route::delete('/parametre', 'destroy')->name('parametre.destroy');
        });

        Route::controller(RecruteurController::class)->group(function() {
            // Route::get('/moi/profils', 'profils')->name('auth.profils');
            // Route::get('/moi/parametres', 'parametres')->name('auth.parametres');
            Route::post('/logout', 'logout')->name('auth.logout');
        });
    });
});

Route::prefix('candidat')->name('candidat.')->group(function () {

    Route::middleware(['guest', 'PreventBackHistory'])->group(function () {
        Route::view('/login', 'candidat.auth.login')->name('login');
        Route::view('/register', 'candidat.auth.register')->name('register');
        Route::post('/register', [CandidatController::class, 'storeRegister'])->name('store.register');
        Route::post('/check', [CandidatController::class, 'checkLogin'])->name('check.login');
    });

    Route::middleware(['auth', 'auth-role:candidat', 'PreventBackHistory'])->group(function () {

        Route::controller(OffreController::class)->group(function () {
            Route::get('/offres', 'index')->name('auth.offres');
            Route::get('/offres/favories', 'offresFavories')->name('auth.offres.favories');
            Route::get('/offres/postulated', 'offer_postuled')->name('auth.offres.postulated');
            Route::get('/offres/{offre}', 'show')->name('auth.offres.show');
            Route::get('/offres/{offre}/postulate', 'postulate')->name('auth.offres.postulate');
            Route::get('/offres/{offre}/cancel', 'cancel_offer')->name('auth.offres.cancel');
        });

        Route::controller(ConversationController::class)->group(function () {
            Route::get('/conversations', 'index')->name('auth.conversations');
            Route::get('/conversations/{conversation}', 'show')->name('auth.conversations.show');
        });

        Route::controller(CvController::class)->group(function () {
            Route::get('/cvs', 'index')->name('auth.cvs');
            Route::post('/cvs/store', 'store')->name('auth.cvs.store');
            Route::get('/cvs/show/{cv}', 'show')->name('auth.cvs.show');
            Route::delete('/cvs/delete{cv}', 'destroy')->name('auth.cvs.destroy');
        });


        Route::controller(ProfileController::class)->group(function() {
            Route::get('/moi/profils', 'index')->name('auth.profils');
            Route::get('moi/profils/edit', 'edit')->name('auth.profils.edit');
            Route::patch('moi/profils/update', 'update')->name('auth.profils.update');
            Route::patch('moi/profils/update-additional', 'updateAdditional')->name('auth.profils.updateAdditional');
        });

        Route::controller(ParametreController::class)->group(function() {
            Route::get('/moi/parametres', 'edit')->name('auth.parametres');
            Route::patch('/parametre', 'update')->name('parametre.update');
            Route::delete('/parametre', 'destroy')->name('parametre.destroy');
        });

        Route::controller(ProfileController::class)->group(function() {
            Route::get('/moi/profils', 'index')->name('auth.profils');
            Route::get('moi/profils/edit', 'edit')->name('auth.profils.edit');
            Route::patch('moi/profils/update', 'update')->name('auth.profils.update');
            Route::patch('moi/profils/update-additional', 'updateAdditional')->name('auth.profils.updateAdditional');
        });

        Route::controller(ParametreController::class)->group(function() {
            Route::get('/moi/parametres', 'edit')->name('auth.parametres');
            Route::patch('/parametre', 'update')->name('parametre.update');
            Route::delete('/parametre', 'destroy')->name('parametre.destroy');
        });

        Route::controller(CandidatController::class)->group(function() {
            // Route::get('/moi/profils', 'profils')->name('auth.profils');
            // Route::get('/moi/parametres', 'parametres')->name('auth.parametres');
            Route::post('/logout', 'logout')->name('auth.logout');
        });
    });
});

Route::get('guest/offres', [OffreController::class, 'guestOffers'])->name('guest-offers');
Route::get('guest/offres/{offre}', [OffreController::class, 'guestShowOffer'])->name('guest-offers.show');


// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

require __DIR__ . '/auth.php';
